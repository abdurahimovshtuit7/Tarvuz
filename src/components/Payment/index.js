import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import styles from './styles';
import Post from '../../assets/SVG/Post';
import Arrow from '../../assets/SVG/Categories/Arrow';
import Promote from '../../assets/SVG/Profile/Promote'
import Archive from '../../assets/SVG/Profile/Archive'

class Item extends Component {
    render() {
        const {title, touch, color, item,number,date} = this.props;
        return (
            <View>
                <View style={styles().container}>
                    <View style={styles().row}>

                        <View style={styles().view}>
                            <Text style={styles(color).text} numberOfLines={1}>
                                {title}
                            </Text>
                            <Text style={styles().text2} numberOfLines={1}>{item}</Text>
                            <Text style={styles().sum} numberOfLines={1}>{date} <Text style={styles().number}>
                                {number}
                            </Text></Text>
                        </View>
                        <Image
                            source={require('../../assets/example/img3.png')}
                            style={styles().image}
                        />
                    </View>
                    </View>
            </View>
        );
    }
}

export default Item;
