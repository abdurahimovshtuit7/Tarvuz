import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


const styles = (props) => StyleSheet.create({
    container: {
        marginHorizontal:15,
        marginVertical:10,
        backgroundColor:colors.white,
        borderRadius:10,
        elevation:2
    },
    boxStyle: {
        paddingVertical:20,
        borderRadius:10,
        backgroundColor:props,
        // borderColor:colors.border

    },
    image:{
        margin:10,
        borderRadius:5,
        overflow:'hidden'
    },

    row:{
        flex:1,
        flexDirection:'row',
        // alignItems:'center',
        justifyContent:'space-between'
    } ,
    view:{
        width: '63%',
        marginLeft:15,
    },
    text:{
        fontSize:18,
        marginTop:15,
        color:props,
        fontWeight:'700',

    },
    text2:{
        fontSize:12,
        marginTop:7,
        color:colors.second,
        fontWeight:'500',
        // marginLeft:15,
    },
    sum:{
        fontSize:14,
        marginTop:9,
        color:colors.primary,
        fontWeight:'700',
    },
    number:{
        fontSize:14,
        // marginTop:9,
        color:colors.second,
        fontWeight:'500',
    },

    component:{
        borderRightWidth:1,
        borderColor:colors.border,
        width: '25%'
    },






})
export default styles
