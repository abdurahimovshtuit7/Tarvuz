import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        // flex: 1,
        //alignItems: 'center',
        //justifyContent: 'space-between',
        // backgroundColor:colors.white,
        // borderWidth:1,

    },
    button:{
        flexDirection:'row',
        marginHorizontal:"10%",
        paddingVertical:18,
        justifyContent:'center',
        // alignItems:'center',
        borderRadius:18,
        marginBottom:"4%",
       // marginTop:'8%',
        backgroundColor: colors.brandColor,
        elevation:30
    },
    text:{
        color:colors.white,
        fontSize:16,
        fontWeight:'600',
        alignSelf:'center'
    }

})

export default styles
