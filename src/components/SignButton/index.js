import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity
} from "react-native";
import styles from './styles'
import colors from "../../assets/styles/colors";
//import NavigationService from "../../navigators/NavigationService";

//import Logo from '../../assets/SVG/Logo'
import Apple from '../../assets/SVG/Apple'

class SignButton extends Component {
    render(){
        const {text,margin,touch}=this.props
        return(
            <View style={styles.container}>
                <TouchableOpacity style={[styles.button,{marginTop:margin}]} onPress={touch}>
                    <Text style={styles.text}>{text}</Text>
                </TouchableOpacity>
            </View>
        )
    }

}

export default SignButton
