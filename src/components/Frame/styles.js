import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        backgroundColor:colors.background,
        // borderWidth:1,
        marginRight:8,
        borderRadius:5,
        paddingHorizontal:10,
        paddingVertical:10,
        marginVertical:3,
    },
    text:{
        color:colors.second,
        fontSize:12,
    },
    dark:{
        color:colors.primary
    }


})

export default styles
