import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity
} from "react-native";
import styles from './styles'
import colors from "../../assets/styles/colors";

//import NavigationService from "../../navigators/NavigationService";


class Button extends Component {
    render(){
        const {text ,number}=this.props
        return(
            <View style={styles.container}>
                 <Text style={styles.text}>
                     {text} <Text style={styles.dark}>
                     {number}
                     </Text>
                 </Text>
            </View>
        )
    }

}

export default Button
