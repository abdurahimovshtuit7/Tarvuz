import React,{Component} from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    Image
} from "react-native";
import styles from './styles'
import Arrow from '../../assets/SVG/Categories/Arrow'
import Colors from "../../assets/styles/colors";


class Item extends Component {
    getAvatar = (index) => {

        let avatarColor;

        const colors = [
            Colors.property,
            Colors.transport,
            Colors.lamp,
            Colors.animal,
            Colors.sport,
            Colors.work,
            Colors.property,
            Colors.lamp,
        ];
        avatarColor = colors[index];

        return { avatarColor }
    };

    render(){
        const {title,text,index,touch,name,color,image} = this.props
        const backGround = this.getAvatar(index)

        return(
            <View>
                <View style={styles(color).container}>
                    <TouchableOpacity style={[styles(color).boxStyle,{backgroundColor:backGround.avatarColor}]} onPress={()=>{
                        touch()
                    }} >
                        <Image style={[styles().image,{opacity:10,backgroundColor:backGround.avatarColor}]}
                            resizeMode={"cover"}
                            // resizeMethod={"scale"} // <-------  this helped a lot as OP said
                                   progressiveRenderingEnabled={true} //---- as well as this
                                   source={{uri:`${image}`}}/>
                         <View style={styles().component}>
                            <Text style={[styles(color).text,]}>{text}</Text>
                             {
                                 !name?null:(<Text style={styles(color).text1}>{name}</Text>)
                             }
                         </View>


                    </TouchableOpacity>
                </View>
            </View>

        )
    }
}

export default Item
