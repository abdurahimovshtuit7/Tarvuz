import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


const styles = (props) => StyleSheet.create({
    container: {
        marginHorizontal:14,
        marginVertical:10,
        justifyContent:'center',
        height:70,
        // borderWidth: 1
    },
    boxStyle: {
        flexDirection:'row',
        alignItems:'center',
        borderRadius:10,
         flex: 1,
        backgroundColor:colors.second,
        paddingHorizontal:15,
        paddingVertical: 15,
        // borderColor:props==="#fff"?colors.border:null,
        // borderWidth:props==="#fff"?1:null

    },
    text:{
        paddingHorizontal:15,
        fontSize:16,
        color:colors.primary,
        fontWeight:'600',
        // borderWidth: 1

    },
    text1:{
        paddingTop:5,
        paddingHorizontal:15,
        fontSize:12,
        color:colors.title,
        fontWeight:'500',

    },
    component:{
        width: "90%" ,

    },
    image: {
        overflow: 'hidden',
        borderRadius: 45,
        width: 40,
        // width:"100%",
        height: 40,
        borderWidth:1,
        borderColor:colors.border,
        backgroundColor: colors.backgroundTransparent
    },


})
export default styles
