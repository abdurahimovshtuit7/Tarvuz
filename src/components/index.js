import Input from './Input'
import SignButton from "./SignButton";
import RegButton from './regButton'
import Layout from './Layout'
import Verify from './Verify'
import Circle from './Circle'
import AddTabButton from './AddTabButton'
import UnderlineTab from './UnderlineTab'
import Item from './Item'
import Card from './Card'
import CategoryCard from './CategoryCard'
import CardList from './CardList'
import AddButton from "./AddButton";
import SwiperLayout from './Swiper'
import UserImage from './UserImage'
import Balance from './Balance'
import ProfileButton from './ProfileButton'
import ArchiveButton from './ArchiveButton'
import Payment from './Payment'
import Switch from './Switch'
import RadioButton from './RadioButton'
import Funds from './Funds'
import Add from './Add'
import Promote from './Promote'
import MessageButton from './MessageButton'
import SendMessage from './SendMessage'
import Frame from './Frame'
import Owner from './Owner'
import TextField from './TextField'
import InfinityFlatList from "./FlatListInfinity";

export default {
    Input,
    SignButton,
    RegButton,
    Layout,
    Verify,
    Circle,
    AddTabButton,
    UnderlineTab,
    Item,
    Card,
    CategoryCard,
    CardList,
    AddButton,
    SwiperLayout,
    UserImage,
    Balance,
    ProfileButton,
    ArchiveButton,
    Payment,
    Switch,
    RadioButton,
    Funds,
    Add,
    Promote,
    MessageButton,
    SendMessage,
    Frame,
    Owner,
    TextField,
    InfinityFlatList
}
