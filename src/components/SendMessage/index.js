import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity
} from "react-native";
import styles from './styles'
import colors from "../../assets/styles/colors";
import  SMS from '../../assets/SVG/TabIcon/SMS'
import Call from '../../assets/SVG/Call'

//import NavigationService from "../../navigators/NavigationService";


class Index extends Component {
    render(){
        const {logo,text}=this.props
        return(
            <View style={styles.container}>
              <TouchableOpacity style={styles.call}>
                  <Call width={11} height={11}/>
                    <Text style={styles.callText}>Call</Text>
              </TouchableOpacity>
                <TouchableOpacity style={styles.message}>
                    <SMS color={colors.white} height={11} width={11}/>
                    <Text style={styles.sendText}>
                    SendMessage
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }

}

export default Index
