import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        // backgroundColor:colors.white,
        // borderWidth:1,
        flexDirection:'row',
        flex:1,
        paddingVertical:15,
        paddingHorizontal: 15,
    },
    call:{
        borderWidth: 1,
        width:'28%',
        marginRight:10,
        borderRadius:15,
        alignItems:'center',
        flexDirection: 'row',
        justifyContent:'center',
        borderColor:colors.brandColor

    },
    message:{
        // borderWidth:1,
        alignItems:'center',
        justifyContent:'center',
        width: "69%",
        borderRadius:15,
        flexDirection:'row',
        backgroundColor:colors.brandColor
    },
    callText:{
       paddingVertical:15,
       marginLeft:10,
        color:colors.brandColor,
        fontSize:14,
        fontWeight:'700'
    },
    sendText:{
        paddingVertical: 15,
        marginLeft:10,
        color:colors.white,
        fontSize:14,
        fontWeight:'700'
    }


})

export default styles
