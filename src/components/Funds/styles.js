import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


const styles = StyleSheet.create({
    view:{
        marginVertical:16,
        borderWidth: 1,
        marginHorizontal: 15,
        borderColor:colors.border,
        borderRadius: 10,
        backgroundColor:colors.white,
        elevation:7
    },
    container: {
        // marginBottom: 35,
        // marginTop:27,
        paddingVertical:25,
        paddingHorizontal:20,
        alignItems: 'center',
        flexDirection: 'row',
        // borderWidth:1,
        justifyContent: 'space-between',
    },
    text:{
        fontSize:14,
        marginLeft:12,
        // marginTop:15,
        color:colors.primary,
        fontWeight:'600',
    },
    radioCircle: {
        height: 25,
        width: 25,
        borderRadius: 100,
        borderWidth: 1,
        borderColor: colors.border,
        alignItems: 'center',
        justifyContent: 'center',
    },
    radioTouch: {
        height: 25,
        width: '25%',
        justifyContent: 'center',
    },
    text1:{
        color:colors.second,
        fontSize: 14,
    },
    row:{
        flexDirection: 'row',
        // borderWidth:1

    },
    border:{
        borderWidth:1,
        flex:1,
        borderColor:colors.border,
        marginLeft:20,

    },

});

export default styles
