import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image, ScrollView} from 'react-native';
import styles from './styles';
import RadioButtonRN from "radio-buttons-react-native";
import Icon from 'react-native-vector-icons/MaterialIcons';
import colors from "../../assets/styles/colors";


class Item extends Component {
    state = {
        value: null,
    };

    render() {
        const {PROP} = this.props;
        const {value} = this.state;

        return (
            <View style={styles.view}>
                {PROP.map(res => {
                    return (
                        <View key={res.key}>
                        <TouchableOpacity  style={styles.container}
                                          onPress={() => {
                                              this.setState({
                                                  value: res.key,
                                              });
                                          }}
                        >
                            <View style={styles.row}>
                                <View style={styles.radioTouch}>
                                    {value === res.key ? (
                                        <Icon name={'check-circle'} color={colors.brandColor} size={26}
                                              style={{margin: 0}}/>) : (<View style={styles.radioCircle}/>)}
                                </View>
                                <View>
                                    <Text style={styles.text}>{res.text}</Text>
                                </View>
                            </View>
                            <Text style={styles.text1}>
                                Add to balance
                            </Text>

                        </TouchableOpacity>
                            { res.key==='4'?null:(<View style={styles.border} />)}
                        </View>
                    );
                })}

            </View>
        );
    }
}

export default Item;
