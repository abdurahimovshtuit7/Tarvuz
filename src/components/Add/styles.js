import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        // justifyContent: 'flex-end',
        // marginTop:'42%'
    },
    button:{
        flexDirection:'row',
        marginHorizontal:15,
        paddingVertical:18,
        justifyContent:'center',
        // alignItems:'center',
        borderRadius:18,
        marginBottom:"4%",
       // marginTop:'8%',
        backgroundColor: colors.brandColor,
        elevation:5
    },
    text:{
        color:colors.white,
        fontSize:16,
        fontWeight:'600',
        alignSelf:'center'
    }

})

export default styles
