import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


const styles = (props) => StyleSheet.create({
    container: {
        marginHorizontal:15,
        marginVertical:10,
        backgroundColor:colors.white,
        borderRadius:10,
        elevation:2
    },
    boxStyle: {
        paddingVertical:20,
        borderRadius:10,
        backgroundColor:props,
        // borderColor:colors.border

    },
    image:{
        margin:5,
        borderRadius:5,
        overflow:'hidden'
    },

    row:{
        flex:1,
        flexDirection:'row',
        // alignItems:'center',
    } ,
    view:{
        width: '60%'
    },
    text:{
        fontSize:14,
        marginTop:15,
        color:colors.primary,
        fontWeight:'700',
        marginLeft:10,
    },
    text2:{
        fontSize:12,
        marginTop:5,
        color:colors.second,
        fontWeight:'500',
        marginLeft:10,
    },
    sum:{
        fontSize:18,
        marginTop:18,
        color:colors.primary,
        fontWeight:'700',
        marginLeft:10,
    },

    bottomText:{
        color:colors.brandColor,
        paddingTop: 6,
        paddingHorizontal: 10,
        fontSize:12
    },
    border:{
        borderWidth:0.5,
        flex:1,
        borderColor:colors.border,

    },

    component:{
        borderRightWidth:1,
        borderColor:colors.border,
        width: '25%'
    },
    text3:{
        paddingTop:10,
        paddingLeft:10,
        fontSize:12,
        color:colors.second,
        paddingBottom:5
    },
    number:{
        fontSize:16,
        fontWeight:'700',
        color:colors.primary,
        paddingLeft:10,
        paddingBottom: 12
    },
    row2:{
        flex:1,
        flexDirection:'row',
        // alignItems:'center',
        paddingHorizontal: 5,
        justifyContent: 'space-between',
        marginVertical: 5
    } ,
    button:{
        width:'49.3%',
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:colors.brandColor,
        borderRadius:5
    },
    button2:{
        width:'49.3%',
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:colors.white,
        borderRadius:5,
        borderWidth: 1,
        borderColor:colors.border
    },
    buttonText:{
        color:colors.white,
        paddingVertical: 16,
        marginLeft:10
        // width:'45%'
    },
    buttonText2:{
        color:props,
        paddingVertical: 16,
        marginLeft:10
    }


})
export default styles
