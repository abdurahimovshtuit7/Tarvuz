import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import styles from './styles';
import Post from '../../assets/SVG/Post';
import Arrow from '../../assets/SVG/Categories/Arrow';
import Promote from '../../assets/SVG/Profile/Promote'
import Archive from '../../assets/SVG/Profile/Archive'

class Item extends Component {
  render() {
    const {title, touch, color, item,balance,icon1,icon2,text1,text2} = this.props;
    return (
      <View>
        <View style={styles().container}>
          <View style={styles().row}>
            <Image
              source={require('../../assets/example/img1.png')}
              style={styles().image}
            />
            <View style={styles().view}>
              <Text style={styles().text} numberOfLines={1}>
                  {title}
              </Text>
              <Text style={styles().text2}>{item}</Text>
              <Text style={styles().sum}>{balance}</Text>
            </View>
          </View>
          <View style={styles().border} />
          <View style={styles().row}>
            <View style={styles().component}>
              <Text style={styles().text3} numberOfLines={1}>
                Views
              </Text>
              <Text style={styles().number} numberOfLines={1}>
                194
              </Text>
            </View>
            <View style={styles().component}>
              <Text style={styles().text3} numberOfLines={1}>
                Saved
              </Text>
              <Text style={styles().number} numberOfLines={1}>
                55
              </Text>
            </View>
            <View style={styles().component}>
              <Text style={styles().text3} numberOfLines={1}>
                Phone views
              </Text>
              <Text style={styles().number} numberOfLines={1}>
                86
              </Text>
            </View>
            <View style={styles().component}>
              <Text style={styles().text3} numberOfLines={1}>
                Messages
              </Text>
              <Text style={styles().number} numberOfLines={1}>
                12
              </Text>
            </View>
          </View>

          <View style={styles().border} />
          <View style={styles().row2}>
              <TouchableOpacity style={styles().button} onPress={touch}>
                  {icon1}
                <Text style={styles().buttonText}>{text1}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles().button2}>
                  {icon2}
                  <Text style={styles(color).buttonText2}>{text2}</Text>
              </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default Item;
