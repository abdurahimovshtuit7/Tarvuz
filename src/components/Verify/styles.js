import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'

export const CELL_SIZE = 50;
export const CELL_BORDER_RADIUS = 15;
export const DEFAULT_CELL_BG_COLOR = '#fff';
export const NOT_EMPTY_CELL_BG_COLOR = colors.brandColor;
export const ACTIVE_CELL_BG_COLOR = '#fff';

export const DEFAULT_CELL_BR_COLOR = colors.border;
export const NOT_EMPTY_CELL_BR_COLOR = colors.border;
export const ACTIVE_CELL_BR_COLOR = colors.brandColor;


const styles = StyleSheet.create({
    codeFieldRoot: {
        height: CELL_SIZE,
        // marginTop: 30,
        // paddingHorizontal: 20,
        justifyContent: 'center',
        // borderWidth: 1,
        // borderRadius: CELL_BORDER_RADIUS,

    },
    cell: {
        marginHorizontal: 8,
        height: CELL_SIZE,
        width: CELL_SIZE,
        lineHeight: CELL_SIZE - 6,
        ...Platform.select({web: {lineHeight: 65}}),
        fontSize: 16,
        textAlign: 'center',
        borderRadius: CELL_BORDER_RADIUS,
        color: colors.primary,
        backgroundColor: '#fff',
        borderColor:colors.border,
        borderWidth:1,

        // IOS
        // shadowColor: '#000',
        // shadowOffset: {
        //     width: 0,
        //     height: 1,
        // },
        // shadowOpacity: 0.22,
        // shadowRadius: 2.22,

        // Android
        // elevation: 3,
    },

    // =======================

    root: {
        minHeight: 800,
        paddingHorizontal: 20,
    },


    dot:{
        color:colors.border,
    }
});

export default styles;
