import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image,Switch} from 'react-native';
import styles from './styles';

class Item extends Component {
    state = {switchValue:false}
    toggleSwitch = (value) => {
        //onValueChange of the switch this function will be called
        this.setState({switchValue: value})
        //state changes according to switch
        //which will result in re-render the text
    }
    render() {
        const {title, touch, color, item,number,date} = this.props;
        return (
            <View>
                <View style={styles().container}>
                    <View style={styles().row}>
                       <Text style={styles().text}>
                           {title}
                       </Text>
                        <Switch
                            style={{}}
                            onValueChange = {this.toggleSwitch}
                            value = {this.state.switchValue}/>
                    </View>
                    <Text style={styles().text2}>
                        {item}
                    </Text>
                </View>
            </View>
        );
    }
}

export default Item;
