import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


const styles = (props) => StyleSheet.create({
    container: {
        // marginHorizontal:15,
        // marginVertical:10,
        // backgroundColor:colors.white,
        // borderRadius:10,
        elevation:2
    },


    row:{
        flex:1,
        flexDirection:'row',
        // alignItems:'center',
        justifyContent:'space-between',
        paddingTop:20,
    } ,

    text:{
        fontSize:16,
        // marginTop:15,
        color:props,
        fontWeight:'700',


    },
    text2:{
        width:'90%',
        fontSize: 14,
        color:colors.second,
        paddingTop:13,
        paddingBottom:20,
        // textAlign:'justify',
        lineHeight: 23,
    }

})
export default styles
