import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'
import {Dimensions} from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


const styles = (props) => StyleSheet.create({
    container: {

        marginHorizontal: 8,
        marginVertical: 10,
        // borderWidth:1,
        // justifyContent:'center',
        // width: "100%",

    },
    boxStyle: {
        // borderWidth:1,
        borderRadius: 10,
        width: 0.43 * Width,
        // height: 102,
        // flex: 1,
        // width:"100%",
        backgroundColor: props,
        // elevation:5,
        // width: "100%",
        paddingHorizontal: 15,
        paddingVertical: 15,
        // opacity:0.2,
        borderColor: props === "#fff" ? colors.border : null,
        borderWidth: props === "#fff" ? 1 : null

    },
    text: {
        // paddingHorizontal:10,
        // paddingTop:8,
        fontSize: 14,
        color: colors.primary,
        fontWeight: '600',
        width: "87%",
        // borderWidth: 1,
        // height: 40,
        justifyContent: 'flex-end'

    },
    row: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 10,
        alignItems: 'flex-end',
        // borderWidth: 1,
        height: 40,
        // width: "47%" ,
        // paddingHorizontal: 10,
    },
    image: {
        overflow: 'hidden',
        borderRadius: 45,
        width: 40,
        // alignItems: 'center',
        // width:"100%",
        height: 40,
        // borderWidth: 1,
        borderColor: colors.border,
        backgroundColor: colors.backgroundTransparent
    },
    arrow:{
        // borderWidth:1,
        marginBottom:3,
    }


})
export default styles
