import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image,ImageBackground} from 'react-native';
import styles from './styles';
import Components from '../index'

class Item extends Component {
    render() {
        const {title, touch} = this.props;
        return (
            <View>
                <TouchableOpacity style={styles().container} onPress={touch}>
                    <View style={styles().row}>
                     <View style={styles().imageRow}>
                        <Image
                            source={require('../../assets/example/img1.png')}
                            style={styles().image}
                        />
                            <Components.UserImage
                                style={styles().icon}
                                // uri={ require('../../assets/images/Ellipse.png')}
                                username={"Sobir Rahimov"}
                                textStyle={styles().iconText}
                            />

                     </View>
                        <View style={styles().view}>
                            <View style={styles().rows}>
                                <Text style={styles().text} numberOfLines={1}>
                                    Sobir Rahimov
                                </Text>
                                <Text style={styles().date} numberOfLines={1}>
                                    Jun 12, 16:55
                                </Text>
                            </View>
                            <Text style={styles().text2} numberOfLines={1}>Roommate needed, Coney Island </Text>
                            <Text style={styles().sum} numberOfLines={1}>Assalom aleykum, albatta</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

export default Item;
