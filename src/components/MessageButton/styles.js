import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


const styles = (props) => StyleSheet.create({
    container: {
        // marginHorizontal:15,
        marginLeft:20,
        marginVertical:10,
        paddingVertical:17,
        borderBottomWidth:1,
        borderColor: colors.border ,
        // alignItems:'center'
        justifyContent: 'center',
        // borderWidth:1
    },
    image:{
        margin:7,
        borderRadius:10,
        width:70,
        height:70,
        overflow:'hidden'
    },
    imageRow:{
        flexDirection:'row',
        // borderWidth:1,
    },

    row:{
        flex:1,
        flexDirection:'row',
        // borderWidth:1,
        alignItems:'center'
    } ,
    rows:{
        // marginVertical:5,
        marginTop:5,
        // flex:1,
        flexDirection:'row',
        justifyContent:'space-between',
        // borderWidth: 1,
        // paddingBottom:0
    } ,
    view:{
        width: '71%',
        marginLeft:10,
    },
    text:{
        fontSize:14,
        color:colors.primary,
        fontWeight:'700',
        width:'67%'

    },
    text2:{
        fontSize:14,
        marginTop:5,
        color:colors.primary,
        fontWeight:'500',
        width:'85%'
        // marginLeft:10,
        // borderWidth:1
    },
    date:{
        fontSize:12,
        color:colors.second,

    },
    sum:{
        fontSize:14,
        marginTop:13,
        color:colors.second,
        // paddingBottom:3,
        width:'85%'
    },
    border:{
        borderWidth:0.5,
        flex:1,
        borderColor:colors.border,

    },
   icon:{
        position: "absolute",
        right: 0,
        top:0,
        borderWidth:3,
        width:35,
        height:35,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:20,
        borderColor:'white',

    },
    iconText:{
        fontSize:12
    }


})
export default styles
