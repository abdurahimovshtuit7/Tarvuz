import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


const styles = (props) => StyleSheet.create({
    container: {
        // flex:1,
        justifyContent:'center',
    },
    boxStyle: {
        flexDirection:'row',
        alignItems:'center',
        flex: 1,
        backgroundColor:props,
        paddingHorizontal:15,
        paddingVertical: 15,

    },
    text:{
        fontSize:18,
        color:colors.primary,
        fontWeight:'700',
        // borderWidth: 1
    },
    text1:{
        fontSize:14,
        color:colors.second,
        fontWeight:'600',
        // borderWidth: 1
    },
    text2:{
        paddingTop:13,
        fontSize:12,
        color:colors.green,
        fontWeight:'600',
        // borderWidth: 1
    },
    icon:{
        // position: "absolute",
        // right: 0,
        // top:0,
        borderWidth:3,
        width:70,
        height:70,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:40,
        borderColor:'white',
    },
    iconText:{
        fontSize:22
    },
    component:{
        flex:1,
        paddingHorizontal:15,
    }
})
export default styles
