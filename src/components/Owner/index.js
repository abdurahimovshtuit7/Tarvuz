import React,{Component} from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    Image
} from "react-native";
import styles from './styles'
import Components from 'components'
import Arrow from '../../assets/SVG/Categories/Arrow'


class Item extends Component {
    render(){
        const {name,listings,status,touch,color} = this.props
        return(
            <View>
                <View style={styles(color).container}>
                    <TouchableOpacity style={styles(color).boxStyle} onPress={
                        touch
                    }>
                        <View style={styles(color).view}>
                           <Components.UserImage
                               style={styles().icon}
                               // uri={ require('../../assets/images/Ellipse.png')}
                               username={name}
                               textStyle={styles().iconText}
                           />
                        </View>
                        <View style={styles().component}>
                        <Text style={styles(color).text} numberOfLines={1}>{name}</Text>
                        <Text style={styles(color).text1} numberOfLines={1}>{listings}</Text>
                        <Text style={styles(color).text2} numberOfLines={1}>{status}</Text>
                        </View>


                    </TouchableOpacity>
                </View>
            </View>

        )
    }
}

export default Item
