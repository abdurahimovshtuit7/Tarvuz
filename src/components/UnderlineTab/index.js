import React, {Component} from 'react';
import {
    Text,
    View,
    ScrollView
} from 'react-native';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import TabBar from 'react-native-underline-tabbar';
import styles from './styles';
import colors from "../../assets/styles/colors";

const Page = ({label}) => (
    <View style={styles.container}>
    {/*//     <Text style={styles.welcome}>*/}
    {/*//         {label}*/}
    {/*//     </Text>*/}
    {/*//     <Text style={styles.instructions}>*/}
    {/*//         To get started, edit index.ios.js*/}
    {/*//     </Text>*/}
    {/*//     <Text style={styles.instructions}>*/}
    {/*//         Press Cmd+R to reload,{'\n'}*/}
    {/*//         Cmd+D or shake for dev menu*/}
    {/*//     </Text>*/}
     </View>
);

class example extends Component {
    render() {
        return (
            <View style={[styles.container]}>
                <ScrollableTabView tabBarActiveTextColor={colors.brandColor}
                                   // scrollWithoutAnimation={true}
                                   renderTabBar={() => <TabBar
                                       underlineColor={colors.brandColor}
                                       underlineHeight={2}
                                       underlineBottomPosition={0}
                                       tabBarStyle={{ backgroundColor: "#fff", borderWidth:0 }}
                                       activeTabTextStyle={{color:colors.primary}}
                                       tabBarTextStyle={{color:colors.second,fontSize:18,fontWeight:'bold'}}
                                   />}>
                    <Page tabLabel={{label: "Recommended"}} label="1"/>
                    <Page tabLabel={{label: "New listings"}} label="2"/>

                </ScrollableTabView>

            </View>
        );
    }
}

export default example
