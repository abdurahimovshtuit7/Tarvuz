import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'


const styles = StyleSheet.create({
        container: {
            // flex: 1,
            flexGrow:1,
            // justifyContent: 'center',
            // alignItems: 'center',
            backgroundColor: colors.white,
            // borderWidth:1,
            marginVertical:10
        },
        welcome: {
            fontSize: 20,
            textAlign: 'center',
            margin: 10,

        },
        instructions: {
            textAlign: 'center',
            color: '#333333',
            marginBottom: 5,
            fontSize: 28,
        },

});

export default styles;
