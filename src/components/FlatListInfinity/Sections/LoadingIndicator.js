import React from 'react';
import PropTypes from 'prop-types';
import { ActivityIndicator, View } from 'react-native';
import colors from "assets/styles/colors";

const LoadingIndicator = ({containerStyle, size,color}) => (
    <View style={containerStyle}>
        <ActivityIndicator animating size={size} color={color}/>
    </View>
);

LoadingIndicator.propTypes = {
    size: PropTypes.oneOf(['small', 'large']),
    containerStyle: PropTypes.object,
    color:PropTypes.object,
};

LoadingIndicator.defaultProps = {
    size: 'large',
    color: colors.brandColor,

    containerStyle: {
        flex: 1,
        // width:'100%',
        // position:'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        // color:colors.brandColor,
        backgroundColor: 'transparent',
        // marginBottom:20
    }
};

export default LoadingIndicator;
