import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'


const styles = StyleSheet.create({
    textInput: {
        flexDirection:'row',
        borderWidth: 1,
        marginHorizontal:"10%",
        paddingVertical:18,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:18,
        borderColor:colors.border,
        marginBottom:"5%",
        paddingHorizontal:20,
        fontSize: 16,
        color:colors.primary,

    },
    focusedTextInput: {
        flexDirection:'row',
        borderWidth: 1,
        marginHorizontal:"10%",
        paddingVertical:18,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:18,
        marginBottom:"5%",
        paddingHorizontal:20,
        fontSize: 16,
        color:colors.primary,
        borderColor:colors.brandColor,
    },
    error: {
        flexDirection:'row',
        borderWidth: 1,
        marginHorizontal:"10%",
        paddingVertical:18,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:18,
        backgroundColor: 'rgba(242, 61, 91, 0.05)',
        marginBottom:"5%",
        paddingHorizontal:20,
        fontSize: 16,
        color:colors.red,
        borderColor:colors.red,
    },
    container:{
        // flex: 1,
        //alignItems: 'center',
        //justifyContent: 'space-between',
        backgroundColor:colors.white,
        borderWidth:1,

    },
    button:{
        flexDirection:'row',
        borderWidth: 1,
        marginHorizontal:"10%",
        paddingVertical:18,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:18,
        borderColor:colors.border,
        marginBottom:"4%",
        paddingHorizontal:20,
        fontSize: 16,
        color:colors.primary
    },
    text:{
        paddingLeft:10,
        fontSize:16,
        fontWeight:'500'
    },
    errorMessage: {
        fontSize: 12,
        color: '#E66466',
        // marginHorizontal:"10%",
        textAlign:'center',
        // fontFamily: 'Gilroy-Medium',
        // marginVertical: 4
    }

})

export default styles
