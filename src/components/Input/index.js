import React, {Component, useState} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    TextInput
} from "react-native";
import styles from './styles'
import colors from "../../assets/styles/colors";
//import NavigationService from "../../navigators/NavigationService";
import PropTypes from 'prop-types'
import TextInputMask from "react-native-text-input-mask";



const stylesFunction = (focus,error) =>{
    console.log("Errorrr:",error)
        if(focus && !error){
            return styles.focusedTextInput
        } else
            if(error) {
                return styles.error
            }
    else return styles.textInput
}

const Button = (props) => {
    const [hasFocus,setState] = useState(false)
    // const setFocus (hasFocus) {
    //     setState({hasFocus});
    // }
        let {placeHolder,onChangeText,onBlur,value,field,form,hasErrorMessage,keyboards,mask}= props
        let error = form && form.touched[field.name] && form.errors[field.name]
        let err = form.touched[field.name] && form.errors[field.name]
        // console.log("PROPS ",props)
     console.log("ERROR" ,err)
        return(
            <View styla={styles.container}>
                <TextInputMask
                           // style={styles.button}

                           style={[stylesFunction(hasFocus,err)]}
                           onFocus={()=>{setState(true)}}
                           // onBlur={this.setFocus.bind(this, false)}
                           clearButtonMode={'unless-editing'}
                           placeholder={placeHolder}
                           placeholderTextColor={colors.textGray}
                           underlineColorAndroid="#fff"
                           autoCapitalize = 'none'
                           onChangeText={form.handleChange(field.name)}
                           onBlur={()=>{form.handleBlur(field.name), setState(false)
                           }}
                           value={field.value}
                           keyboardType={keyboards}
                           mask={mask}

                           // autoCorrect={false}
                >
                </TextInputMask>

            </View>
        )

}
Button.propTypes = {
    style: PropTypes.object,
    containerStyle: PropTypes.object,
    hasErrorMessage: PropTypes.bool,
    hasLeftText: PropTypes.bool,
    hasUnderLine: PropTypes.bool,
    leftTextStyle: PropTypes.object,
    hairLineStyle: PropTypes.object,
    leftText: PropTypes.string,
    hasRightText: PropTypes.bool,
    rightTextStyle: PropTypes.object,
    rightText: PropTypes.string
};
Button.defaultProps = {
    style: {},
    containerStyle: {},
    hairLineStyle: {},
    hasErrorMessage: false,
    hasUnderLine: true,
    hasLeftText: false,
    leftTextStyle: {},
    leftText: '',
    hasRightText: false,
    rightTextStyle: {},
    rightText: ''
};
export default Button
