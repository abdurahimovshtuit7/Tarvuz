import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


const styles = StyleSheet.create({
    view:{
        marginTop:16,
    },
    container: {
        // marginTop:27,
        padding:15,
        borderWidth:1,
        marginHorizontal:15,
        borderRadius: 10,
        borderColor: colors.border,
        marginVertical:8
    },
    text:{
        fontSize:18,
        // marginTop:15,
        marginBottom:25,
        color:colors.primary,


    },
    text2:{
        fontSize:18,
        fontWeight:'600',
        // width:'50%'
    },
    text3:{
        fontSize:14,
        color:colors.second,
        // width:'60%'
        paddingVertical:5
    },
    text4:{
        fontWeight: '700'
    },
    radioCircle: {
        height: 25,
        width: 25,
        borderRadius: 100,
        borderWidth: 1,
        borderColor: colors.border,
        alignItems: 'center',
        justifyContent: 'center',
        // marginLeft: 3
    },
    radioTouch: {
        height: 25,
        width: '13%',
        //  borderRadius: 100,
        //  borderWidth: 1,
        // borderColor: colors.border,
        alignItems: 'flex-end',
        justifyContent: 'center',
        // marginLeft: 3

    },
    row:{
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    row1:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems:'flex-end'
    },


});

export default styles
