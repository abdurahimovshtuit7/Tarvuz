import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image, ScrollView} from 'react-native';
import styles from './styles';
import RadioButtonRN from "radio-buttons-react-native";
import Icon from 'react-native-vector-icons/MaterialIcons';
import colors from "../../assets/styles/colors";



class Item extends Component {
    state = {
        value: null,
    };
    render() {
        const { PROP } = this.props;
        const { value } = this.state;

        return (
            <View style={styles.view}>
                {PROP.map(res => {
                    return (
                        <TouchableOpacity key={res.key} style={[styles.container,{backgroundColor:(value === res.key?colors.brandColor:colors.white),borderWidth:(value === res.key?null:1)}]}
                                          onPress={() => {
                                              this.setState({
                                                  value: res.key,
                                              });
                                          }}
                        >
                            <View style={styles.row}>
                            <Text style={[styles.text,{color:(value === res.key?colors.white:colors.primary)}]}>
                                {res.title}
                            </Text>
                            <Text style={[styles.text2,{color:(value === res.key?colors.white:colors.brandColor)}]}>
                                {`$ ${res.cost}`}
                            </Text>
                        </View>
                            <View style={styles.row1}>

                                          <View>
                                              <Text style={[styles.text3,{color:(value === res.key?colors.white:colors.second)}]}>{res.array[0].text} <Text style={styles.text4}> {res.array[0].item} </Text></Text>
                                              {res.array[1]?( <Text style={[styles.text3,{color:(value === res.key?colors.white:colors.second)}]}>{res.array[1].text} <Text style={[styles.text4,{}]}> {res.array[1].item} </Text></Text>):null}
                                              {res.array[2]?( <Text style={[styles.text3,{color:(value === res.key?colors.white:colors.second)}]}>{res.array[2].text} </Text>):null}

                                              {/*<Text style={styles.text3}>{res.array[2].text} <Text style={[styles.text4,{}]}> {res.array[2].item} </Text></Text>*/}
                                          </View>

                            <View style={styles.radioTouch}>
                                {value === res.key? (<Icon name={'check-circle'} color={colors.white} size={26} style={{margin:0}} />):(<View style={styles.radioCircle}/>)}
                            </View>
                            </View>
                        </TouchableOpacity>
                    );
                })}

            </View>
        );
    }
}

export default Item;
