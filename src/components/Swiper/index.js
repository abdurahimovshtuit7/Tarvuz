import React, { Component } from 'react';
import {
    ImageBackground,
    Text, TouchableOpacity,
    View,
} from 'react-native';

import Swiper from 'react-native-swiper';
//import SwiperImage from '../../images/button/'

import styles from "./styles";

class SwImage extends Component {

    render(){
        return (
            <View style={styles.container}>
                <Swiper style={styles.wrapper}
                        dot={<View style={styles.dotStyle} />}
                        activeDot={<View style={styles.activeDotStyle} />}
                        paginationStyle={styles.Pagination}
                        loop={true}
                        autoplay={true}
                        // autoplayTimeout={3}
                >
                    <View style={styles.slide1}>
                        <ImageBackground source={require('../../assets/example/AnyConv.com__1.png')} style={styles.images}/>
                    </View>
                    <View style={styles.slide2}>
                        <ImageBackground source={require('../../assets/example/AnyConv.com__3.png')} style={styles.images}/>
                    </View>
                </Swiper>
            </View>
        );
    }
}

export default SwImage;
