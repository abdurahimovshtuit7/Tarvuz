import {Dimensions, StyleSheet} from 'react-native';
var {height, width} = Dimensions.get('window');
const styles = StyleSheet.create({
    container:{
        // marginHorizontal: 17,
        // borderRadius:11,
        // borderWidth:1,
        marginTop:13,
       // position:'absolute',
      //  zIndex:2
      //   paddingBottom:8

    },
    wrapper: {
        height: 30*height/100,
        //borderRadius:10
        // paddingBottom:10
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',

    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',

    },
    dotStyle:{
        backgroundColor: 'rgba(255,255,255,.3)',
        width: 7,
        height: 7,
        borderRadius: 6,
        marginLeft: 3,
        marginRight: 3,
        borderColor:'white',
        borderWidth:1
    },
    activeDotStyle:{
        backgroundColor: '#fff',
        width: 7,
        height: 7,
        borderRadius: 6,
        marginLeft: 3,
        marginRight: 3,
    },
    Pagination:{
        bottom:12
    },
    images:{
        width:0.93*width,
        height: 30*height/100,
        // borderRadius: 15,
        // overflow: "hidden",
    },
})

export default styles
