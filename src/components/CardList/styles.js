import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


const styles = (props) => StyleSheet.create({
    container: {
        marginHorizontal:14,
        marginVertical:10,
        // borderWidth:1,
        justifyContent:'center',
    },
    boxStyle: {
        marginBottom:15,
        borderRadius:10,
        backgroundColor:props,
        paddingBottom: '9%',
        paddingTop:5,
        borderBottomWidth:1,
        borderColor:colors.border

    },
    text:{
        fontSize:16,
        color:colors.primary,
        fontWeight:'500',
        width:'90%'
    },
    row:{
        flex:1,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        paddingHorizontal: 10,
    } ,
    bottomText:{
        color:colors.brandColor,
        paddingTop: 6,
        paddingHorizontal: 10,
        fontSize:12
    }


})
export default styles
