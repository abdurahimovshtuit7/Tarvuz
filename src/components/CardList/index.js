import React,{Component} from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    Image
} from "react-native";
import styles from './styles'
import Arrow from '../../assets/SVG/Categories/Arrow'


class Item extends Component {
    render(){
        const {title,touch,color,item} = this.props
        return(
            <View>
                <View style={styles().container}>
                    <TouchableOpacity style={styles().boxStyle} onPress={()=>{
                        touch()
                    }} >
                        <View style={styles().row}>
                            <Text style={styles().text}>{title}</Text>
                            <Arrow/>
                        </View>
                        <Text style={styles().bottomText}>{item}</Text>



                    </TouchableOpacity>
                </View>
            </View>

        )
    }
}

export default Item
