import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


const styles = (props) => StyleSheet.create({
    container: {
        marginHorizontal:14,
        marginVertical:10,
        justifyContent:'center',
        // height:70,
    },
    boxStyle: {
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        borderRadius:10,
        flex: 1,
        backgroundColor:props,
        paddingHorizontal:20,
        paddingVertical: 20,
    },
    view:{
       width:'69%'
    },
    text:{
        // paddingHorizontal:15,
        fontSize:18,
        color:colors.primary,
        fontWeight:'700',
        // width: "90%" ,
    },
    balance:{
        fontSize: 12,
        color:colors.second,
        paddingBottom:7
    },
    touch:{
        borderRadius: 5,
        backgroundColor:colors.brandColor

    },
    funds:{
        padding:10,
        fontSize:14,
        color:colors.white
    }


})
export default styles
