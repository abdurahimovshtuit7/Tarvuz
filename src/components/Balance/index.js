import React,{Component} from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    Image
} from "react-native";
import styles from './styles'
import Arrow from '../../assets/SVG/Categories/Arrow'


class Item extends Component {
    render(){
        const {title,text,index,touch,name,color,balance} = this.props
        return(
            <View>
                <View style={styles(color).container}>
                    <View style={styles(color).boxStyle}>
                           <View style={styles().view}>
                               <Text style={styles().balance} numberOfLines={1}>
                                   {`${balance} :`}
                               </Text>
                               <Text style={styles().text} numberOfLines={1}>{text}</Text>
                           </View>
                            <TouchableOpacity style={styles().touch} onPress={()=>{
                                touch()
                            }}>
                                 <Text style={styles().funds} numberOfLines={1}>
                                     {name}
                                 </Text>
                            </TouchableOpacity>

                    </View>
                </View>
            </View>

        )
    }
}

export default Item
