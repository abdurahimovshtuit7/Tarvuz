import React, {Component} from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    ImageBackground, Dimensions
} from "react-native";
import styles from './styles'
import SaveIcon from '../../assets/SVG/Save'
// import Megapolis from '../../assets/SVG/MegaPolis'
import Room from '../../assets/SVG/Room'
import NavigationService from "../../navigators/NavigationService";
import colors from "../../assets/styles/colors";
import moment from "moment";

const windowWidth = Dimensions.get('window').width;

class Item extends Component {
    state = {
        isActive: false
    }
    // componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS) {
    //     moment.locale('ru')
    //     // this.bigItem()
    // }


    render() {
        const {index, item, saved, savedId, list, width, bigItem,navigate} = this.props
        const uri = item.avatar ? ({uri: item.avatar}) : require('assets/images/tarvuz.png')
        // const save = savedId?savedId:null
        let isSelected = list[item.id]
        // const exampleImageUri = Image.resolveAssetSource(item.avatar).uri
        return (

            <TouchableOpacity style={[styles.boxStyle, {width: width}]} onPress={() => {
              navigate()
            }}>
                <ImageBackground
                    style={[styles.image, {backgroundColor: `rgba(0, 0, 0, 0.25)`}]}
                    resizeMode={"cover"}
                    // resizeMethod={"scale"} // <-------  this helped a lot as OP said
                    progressiveRenderingEnabled={true} //---- as well as this
                    source={uri}
                >
                    <TouchableOpacity style={styles.box} onPress={() => {
                        let active = this.state.isActive
                        this.setState({isActive: !active})
                        saved(item.id, item)
                    }}>
                        <SaveIcon
                            style={styles.save}
                            color={isSelected ? colors.brandColor : colors.second}
                            opacity={isSelected ? '1' : '0.8'}
                        />
                    </TouchableOpacity>
                    <View style={[styles.textView]}>
                        <Text style={styles.text1}>
                            {
                                moment(item.created_at).format("MMM D")}
                        </Text>
                    </View>
                </ImageBackground>
                <Text style={styles.text}>{`$ ${item.price}`}</Text>

                <Text style={styles.about} numberOfLines={1}>
                    {item.title}
                </Text>

                <Text style={styles.place} numberOfLines={1}>
                    {item.full_address}
                </Text>


            </TouchableOpacity>

        )
    }
}

export default Item
