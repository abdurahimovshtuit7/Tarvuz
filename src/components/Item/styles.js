import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // marginHorizontal:8,
        // marginVertical:10,
        // width:'100%',
        // borderWidth:1,
        // justifyContent:'center',
    },
    boxStyle: {
        // flex:0.48,
        // flexGrow:1,
        // alignSelf: 'stretch',
        // flexWrap:'wrap',
        // borderWidth:1,
        // marginHorizontal:5,
        marginVertical:10,
        borderRadius:10,
        backgroundColor:colors.white,
        borderColor:colors.border,
        elevation:5,
        shadowColor: "#000",
        width: '48%',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

    },
    text:{
         paddingHorizontal:10,
        // paddingTop:8,
         fontSize:18,
         color:colors.primary,
        fontWeight:'600',
        paddingVertical:10
    } ,
    about:{
        paddingHorizontal:10,
        // paddingTop:8,
        fontSize:12,
        color:colors.primary,
        fontWeight:'500',
        // paddingVertical:10
    } ,
    image:{
        overflow:'hidden',
        borderTopLeftRadius:10,
        borderTopRightRadius:10,
        width: '100%',
        // width:"100%",
        height:150,
        // borderWidth:1,
        // paddingHorizontal:44,
        // paddingVertical:30,
        // justifyContent:'center',
        // alignItems:'center',
    },
    place:{
        paddingHorizontal:10,
        // paddingTop:8,
        fontSize:12,
        color:colors.second,
        fontWeight:'500',
        paddingTop:5,
        paddingBottom:8,
    },

    text1:{
        fontSize:12,
        color:'white',
        padding:4,

        alignSelf:'center',

    },
    save:{



    },
    box:{
        position: 'absolute',
        right:7,
        top:7,
        borderRadius:5,
        overflow: 'hidden',
        // borderWidth:1,
        paddingHorizontal:5,
        paddingVertical: 4,
        // backgroundColor:'#fff'
    },
    textView:{
        backgroundColor:`rgba(0, 0, 0, 0.3)`,
        position:'absolute',
        borderRadius:7,
        top:122,
        left:8
    }

})
export default styles
