import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


const styles = StyleSheet.create({
   view:{
       marginTop:16,
   },
    container: {
        // marginBottom: 35,
        marginTop:27,
        paddingVertical:5,
        alignItems: 'center',
        flexDirection: 'row',
        // borderWidth:1,
        justifyContent: 'space-between',
    },
    text:{
        fontSize:16,
        // marginTop:15,
        color:colors.primary,
        fontWeight:'600',
    },
    radioCircle: {
        height: 25,
        width: 25,
        borderRadius: 100,
        borderWidth: 1,
        borderColor: colors.border,
        alignItems: 'center',
        justifyContent: 'center',
        // marginLeft: 3
    },
    radioTouch: {
        height: 25,
        width: '13%',
       //  borderRadius: 100,
       //  borderWidth: 1,
        // borderColor: colors.border,
        alignItems: 'flex-end',
        justifyContent: 'center',
        // marginLeft: 3

    },

});

export default styles
