import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image, ScrollView} from 'react-native';
import styles from './styles';
import RadioButtonRN from "radio-buttons-react-native";
import Icon from 'react-native-vector-icons/MaterialIcons';
import colors from "../../assets/styles/colors";



class Item extends Component {

    render() {
        const { PROP, touch,lang } = this.props;

        return (
            <View style={styles.view}>
                {PROP.map(res => {
                    return (
                        <TouchableOpacity key={res.key} style={styles.container}
                                          onPress={() => {
                                              touch(res.key)
                                          }}
                        >
                            <Text style={styles.text}>{res.text}</Text>
                            <View style={styles.radioTouch}
                               >
                                {lang === res.key? (<Icon name={'check-circle'} color={colors.brandColor} size={26} style={{margin:0}} />):(<View style={styles.radioCircle}/>)}
                            </View>
                        </TouchableOpacity>
                    );
                })}

            </View>
        );
    }
}

export default Item;
