import { StyleSheet } from 'react-native';
import colors from "../../assets/styles/colors";
// import {container} from 'assets/styles/common'

export default StyleSheet.create({
    bigButton: {
        // marginTop: -8,
        alignItems:'center',
        justifyContent:'center',
        paddingVertical:15,
        // borderWidth:1,
        paddingHorizontal:21,
        borderRadius:40,
        backgroundColor:colors.brandColor
    }
});
