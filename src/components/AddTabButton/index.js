import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import Plus from '../../assets/SVG/TabIcon/Plus';
import styles from './styles'
import NavigationService from "../../navigators/NavigationService";
import {connect} from "react-redux";

const AddButton = (props) => {
    let {login,navigation} = props;

    return(
        <TouchableOpacity
            onPress={()=>{
                // if(login && login.token)
                NavigationService.navigate('home');
                //
                // else NavigationService.navigate('profileTab')

            }}
            style={styles.bigButton}
        >
            <Plus/>
        </TouchableOpacity>
    );
};
AddButton.propTypes = {
};
AddButton.defaultProps = {
};
const mapStateToProps = (state, ownProps) => {
    return {
        login: state.profile.data
    };
};
export default connect(mapStateToProps)(AddButton);
