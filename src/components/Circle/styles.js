import {StyleSheet} from 'react-native';
import colors from "../../assets/styles/colors";

const styles = StyleSheet.create({
        container: {
            // height:130,
            // width:90,
            // borderRadius: 100,
            marginTop: '3%',
            marginHorizontal: 8,
            backgroundColor: 'white',
            // elevation:3,
            marginBottom: 2,
            paddingBottom: 40,
            // borderWidth: 1,
            // justifyContent: 'flex-start',
            alignItems: 'flex-start'
        },
        circle: {
            height: 90,
            width: 90,
            justifyContent: 'center',
            alignItems: 'center'
        },
        text: {
            paddingTop: 10,
            textAlign: 'center',
            height:45,
            fontSize: 14,
            // borderWidth:1,
            color: colors.primary
        },
        image: {
            overflow: 'hidden',
            borderRadius: 45,
            width: 90,
            // width:"100%",
            height: 90,
            borderWidth:1,
            borderColor:colors.border,
            backgroundColor: colors.backgroundTransparent
        },
    }
)

export default styles
