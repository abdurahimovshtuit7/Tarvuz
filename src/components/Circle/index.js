import React,{Component} from 'react';
import {
    View,
    TouchableOpacity,
    Text,
    Image, ImageBackground
} from "react-native";
import NavigationService from "../../navigators/NavigationService";
import styles from './styles'
import Colors from "../../assets/styles/colors";
import Translate from 'services/translateTitle'


class Circle extends Component{
   getAvatar = (username) => {
        const userName = username || '';
        const name = userName.toUpperCase().split(' ');
        let avatarName = "";
        let avatarColor;
        if (name.length === 1) {
            avatarName = `${name[0].charAt(0)}`;
        }
        else if (name.length > 1) {
            avatarName = `${name[0].charAt(0)}${name[1].charAt(0)}`;
        }
        else {
            avatarName = '';
        }
        let sumChars = 0;
        for (let i = 0; i < userName.length; i += 1) {
            sumChars += userName.charCodeAt(i);
        }
        const colors = [
            Colors.property,
            Colors.transport,
            Colors.lamp,
            Colors.animal,
            Colors.sport,
            Colors.work,
            Colors.property,
            Colors.lamp,
        ];
        avatarColor = colors[sumChars % colors.length];

        return { avatarColor, avatarName }
    };

    render(){
        const {touch,title,text,image,index,item,lang} =this.props
        let avatar = this.getAvatar(text);
        // let name = Translate.title({lang, item.name_en,item.name_ru,item.name_uz})
        return (
            <View style = {styles.container}>
               <TouchableOpacity style = {styles.circle} onPress={()=>{
                   touch()
               }}>
                   {
                       index==='0'?(title):(
                           <Image style={[styles.image,{ backgroundColor: avatar.avatarColor }]}
                                  resizeMode={"cover"}
                               // resizeMethod={"scale"} // <-------  this helped a lot as OP said
                                  progressiveRenderingEnabled={true} //---- as well as this
                                  source={{uri:`${item.icon_image}`}}
                           />
                       )

                   }
                            <Text style={styles.text} numberOfLines={2} >
                                {text}
                            </Text>
               </TouchableOpacity>
            </View>
        )
    }
}

export default Circle
