import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity
} from "react-native";
import styles from './styles'
import colors from "../../assets/styles/colors";

//import NavigationService from "../../navigators/NavigationService";


class Button extends Component {
    render(){
        const {logo,text}=this.props
        return(
            <View styla={styles.container}>
                <TouchableOpacity style={styles.button}>
                    {logo}
                   <Text style={styles.text}>{text}</Text>

                </TouchableOpacity>
            </View>
        )
    }

}

export default Button
