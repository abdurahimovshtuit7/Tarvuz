import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'

const styles = StyleSheet.create({
    container:{
       // flex: 1,
        //alignItems: 'center',
        //justifyContent: 'space-between',
        backgroundColor:colors.white,
        borderWidth:1,

    },
    button:{
        flexDirection:'row',
        borderWidth: 1,
        marginHorizontal:"10%",
        paddingVertical:18,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:18,
        borderColor:colors.border,
        marginBottom:"4%"
    },
    text:{
        paddingLeft:10,
        fontSize:16,
        fontWeight:'500'
    }

})

export default styles
