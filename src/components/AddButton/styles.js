import { StyleSheet } from 'react-native';
import colors from "../../assets/styles/colors";
// import {container} from 'assets/styles/common'

export default StyleSheet.create({
    bigButton: {
        // marginTop: -8,
        // alignItems:'center',
        justifyContent:'center',
        borderRadius:40,
        backgroundColor:colors.brandColor
    },
    Button:{
        paddingVertical:10,
        paddingHorizontal:21,
        // borderWidth:1
    }
});
