import React, {Component} from 'react';
import {Animated, TouchableHighlight, TouchableOpacity, View} from "react-native";
import styles from "./styles";
import {connect} from "react-redux";
import colors from "assets/styles/colors";


class AddButton extends Component {

    render() {

        const {background,icon,touch,border} = this.props

        return (<Animated.View
            style={[styles.bigButton, {backgroundColor: background,borderColor:(border===colors.brandColor)?border:null,borderWidth:(border)?1.5:null }]}
        >

            <TouchableOpacity
                activeOpacity={1}
                onPress={touch}
                style={[styles.Button]}
                underlayColor="#2882D8"

            >
                <Animated.View>
                    {icon}

                </Animated.View>


            </TouchableOpacity>
        </Animated.View>)
    }
}

const mapStateToProps = (state, ownProps) => {
    return {

    };
};

export default connect(mapStateToProps)(AddButton);
