import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


const styles = (props) => StyleSheet.create({
    container: {
        marginHorizontal:20,
        // marginVertical:10,
        // paddingHorizontal:20,
        // borderWidth:1,
        // justifyContent:'center',

    },
    boxStyle: {
        paddingVertical:20,
        borderRadius:10,
        backgroundColor:props,
        // borderColor:colors.border

    },
    text:{
        fontSize:14,
        color:colors.primary,
        fontWeight:'500',
        width:'65%',
        // marginLeft:15,
        // borderWidth:1,
        marginLeft:15,
    },
    active:{
        // width: 30,
        // height:30,
        justifyContent:'center',
        alignItems: 'flex-end',
        // borderWidth: 1,
        width: '15%',
        marginRight:10,


    },
    activeText:{
        color:colors.white,
        paddingHorizontal:5,
        paddingVertical:2,
        fontSize:10,
        fontWeight: 'bold',
        // width: '3'
        backgroundColor:props,
        borderRadius: 20,
    },
    row:{
        flex:1,
        flexDirection:'row',
        // justifyContent:'center',
        alignItems:'center',
        // paddingHorizontal: 20,
    } ,
    bottomText:{
        color:colors.brandColor,
        paddingTop: 6,
        paddingHorizontal: 10,
        fontSize:12
    }


})
export default styles
