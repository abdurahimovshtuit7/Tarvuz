import React,{Component} from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    Image
} from "react-native";
import styles from './styles'
import Post from '../../assets/SVG/Post'
import Arrow from '../../assets/SVG/Categories/Arrow'


class Item extends Component {
    render(){
        const {title,touch,color,item,active} = this.props
        return(
            <View>
                <View style={styles().container}>
                    <TouchableOpacity style={styles().boxStyle} onPress={()=>{
                        touch()
                    }} >
                        <View style={styles().row}>
                            <Post/>
                            <Text style={styles().text}>{title}</Text>
                            <View style={styles(active).active}>
                                <Text style={styles(active).activeText} numberOfLines={1}>
                                    {item}
                                </Text>
                            </View>
                            <Arrow/>
                        </View>

                    </TouchableOpacity>
                </View>
            </View>

        )
    }
}

export default Item
