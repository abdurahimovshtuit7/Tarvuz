import {StyleSheet} from 'react-native';
import colors from '../assets/styles/colors'

const styles = StyleSheet.create({


    text:{
        color:colors.brandColor,
        fontWeight:'bold',
        paddingRight:15,
        fontSize:16,
        textAlign:'center'
    },


})

export default styles
