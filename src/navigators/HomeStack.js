import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
// import {createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import HomeScreen from "../containers/HomeScreen";
import Categories from "../containers/Categories";
import Category from "../containers/Categories/Category";
import NewListing from "../containers/NewListing";
import {connect} from "react-redux";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();



const HomeStack = (props) => (
    <Stack.Navigator initialRouteName='home'>
        <Stack.Screen name='home' component={HomeScreen}
                      options={{
                          headerShown: false,
                      }}
        />
        <Stack.Screen name='categories' component={Categories}
                      options={({route}) => ({
                          headerShown: false,
                      })}
        />
        <Stack.Screen name='category' component={Category}
                      options={({route}) => ({
                          headerShown: false,

                      })}
                      backTitleVisible={true}
        />

    </Stack.Navigator>
)

const mapStateToProps = (state, ownProps) => {
    return {
        tabButton: state.tabButton.data
    };
};

export default connect(mapStateToProps)(HomeStack);
