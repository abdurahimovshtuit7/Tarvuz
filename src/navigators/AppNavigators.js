/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
// import {createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
    TouchableOpacity,
    Text
} from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

//------------SCREENS--------------
import InitialScreen from '../containers/InitialScreen';
import SignIn from '../containers/SignIn';
import Login from '../containers/Login'
import SignUp from "../containers/SignUp";
import Email from '../containers/Email'
import VerifyPhone from "../containers/VerifyPhone";
import VerifyEmail from "../containers/VerifyEmail";
import NewListing from '../containers/NewListing'
import SingleListing from '../containers/SingleListing'
import ArchiveListing from '../containers/ArchiveListing'
import ActiveListing from "../containers/ActiveListing";
import Payments from '../containers/Payments'
import Settings from "../containers/Settings";
import Promotion from '../containers/Promotion'
import Chat from '../containers/Messages/Chat'
import ListingOwner from 'containers/SingleListing/ListingOwner'
import AddCategory from 'containers/NewListing/Category'
import AddNew from 'containers/NewListing/AddNew'
import AddNewList from 'containers/NewListing/AddNewList'

//--------TabScreen---------
import HomeScreen from "../containers/HomeScreen";
import Saved from '../containers/Saved';
import Add from '../containers/Add';
import Messages from '../containers/Messages';
import Profile from '../containers/Profile'
import TabBar from 'navigators/TabNavigator'

import {connect} from 'react-redux';
import NavigationService from './NavigationService';
import colors from "../assets/styles/colors";
import styles from './styles'

//----------------SVG------------
import House from '../assets/SVG/TabIcon/House'
import Heart from '../assets/SVG/TabIcon/Heart'
import SMS from '../assets/SVG/TabIcon/SMS'
import User from '../assets/SVG/TabIcon/User'
import AddTabButton from "../components/AddTabButton";
import AddFunds from "../containers/AddFunds";


const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();



const StackScreen = (props) => (
    <Stack.Navigator initialRouteName='initialScreen'>
        {/*<Stack.Screen name='initialScreen' component={InitialScreen}*/}
        {/*              options={{*/}
        {/*                  headerShown: false,*/}
        {/*              }}*/}
        {/*/>*/}
        <Stack.Screen name='tab' component={TabBar}
                      options={({route}) => ({
                          headerShown: false,
                      })}
        />
        <Stack.Screen name='signIn' component={SignIn}
                      options={{
                          headerShown: false,
                      }}
                      backTitleVisible={false}
        />
        <Stack.Screen name='login' component={Login}
                      options={{
                          // headerShown: false,
                          headerBackTitle: "",
                          title: "",
                          headerStyle: {
                              backgroundColor: colors.white,
                              elevation: 0
                          },
                          headerTintColor: colors.primary,

                      }}
        />
        <Stack.Screen name='signUp' component={SignUp}
                      options={{
                          // headerShown: false,
                          headerBackTitle: "",
                          title: null,
                          headerStyle: {
                              backgroundColor: colors.white,
                              elevation: 0
                          },
                          headerTintColor: colors.primary,
                          headerRight: () => (
                              <TouchableOpacity
                                  onPress={() => {
                                      NavigationService.navigate('email')
                                  }}>
                                  <Text style={styles.text}>Sign up with Email</Text>
                              </TouchableOpacity>
                          ),
                      }}
        />
        <Stack.Screen name='email' component={Email}
                      options={{
                          // headerShown: false,
                          title: "",
                          headerBackTitle: "",
                          headerStyle: {
                              backgroundColor: colors.white,
                              elevation: 0
                          },
                          headerTintColor: colors.primary,
                          headerRight: () => (
                              <TouchableOpacity
                                  onPress={() => {
                                      NavigationService.navigate('signUp')
                                  }}>
                                  <Text style={styles.text}>Sign up with Phone number</Text>
                              </TouchableOpacity>
                          ),
                      }}
                      backTitleVisible={false}
        />
        <Stack.Screen name='verifyPhone' component={VerifyPhone}
                      options={{
                          // headerShown: false,
                          title: "",
                          headerBackTitle: "",
                          headerStyle: {
                              backgroundColor: colors.white,
                              elevation: 0
                          },
                          headerTintColor: colors.primary,
                      }}
                      backTitleVisible={false}

        />
        <Stack.Screen name='verifyEmail' component={VerifyEmail}
                      options={{
                          // headerShown: false,
                          title: "",
                          headerBackTitle: "",
                          headerStyle: {
                              backgroundColor: colors.white,
                              elevation: 0
                          },
                          headerTintColor: colors.primary,
                      }}
                      backTitleVisible={false}

        />


        <Stack.Screen name='singleListing' component={SingleListing}
                      options={({route}) => ({
                          headerShown: false,
                      })}
        />
        <Stack.Screen name='archive' component={ArchiveListing}
                      options={({route}) => ({
                          // headerShown: false,
                          headerBackTitle: "",
                          title: route.params.title,
                          headerStyle: {
                              backgroundColor: colors.white,
                              elevation: 0
                          },
                          headerTintColor: colors.primary,
                      })}
        />
        <Stack.Screen name='active' component={ActiveListing}
                      options={({route}) => ({
                          // headerShown: false,
                          headerBackTitle: "",
                          title: route.params.title,
                          headerStyle: {
                              backgroundColor: colors.white,
                              elevation: 0
                          },
                          headerTintColor: colors.primary,
                      })}
        />
        <Stack.Screen name='payment' component={Payments}
                      options={({route}) => ({
                          // headerShown: false,
                          headerBackTitle: "",
                          title: route.params.title,
                          headerStyle: {
                              backgroundColor: colors.white,
                              elevation: 0
                          },
                          headerTintColor: colors.primary,
                      })}
        />
        <Stack.Screen name='settings' component={Settings}
                      options={({route}) => ({
                          // headerShown: false,
                          headerBackTitle: "",
                          title: route.params.title,
                          headerStyle: {
                              backgroundColor: colors.white,
                              elevation: 0
                          },
                          headerTintColor: colors.primary,
                      })}
        />
        <Stack.Screen name='addfunds' component={AddFunds}
                      options={({route}) => ({
                          // headerShown: false,
                          title: route.params.title,
                          headerBackTitle: "",
                          headerStyle: {
                              backgroundColor: colors.white,
                              elevation: 0
                          },
                          headerTintColor: colors.primary,
                      })}
        />
        <Stack.Screen name='promotion' component={Promotion}
                      options={{
                          headerBackTitle: "",
                          headerShown: false,
                      }}
                      backTitleVisible={true}
        />
        <Stack.Screen name='chat' component={Chat}
                      options={{
                          headerShown: false,
                      }}
        />
        <Stack.Screen name='owner' component={ListingOwner}
                      options={({route}) => ({
                          // headerShown: false,
                          title: route.params.title,
                          headerStyle: {
                              backgroundColor: colors.white,
                              elevation: 0
                          },
                          headerTintColor: colors.primary,
                      })}
        />
        <Stack.Screen name='newlisting' component={NewListing}
                      options={{
                          headerShown: false,

                      }}
                      backTitleVisible={true}
        />
        <Stack.Screen name='addCategory' component={AddCategory}
                      options={{
                          headerShown: false,

                      }}
                      backTitleVisible={true}
        />
        <Stack.Screen name='addNew' component={AddNew}
                      options={{
                          headerShown: false,

                      }}
                      backTitleVisible={true}
        />
        <Stack.Screen name='addNewList' component={AddNewList}
                      options={{
                          headerShown: false,

                      }}
                      backTitleVisible={true}
        />

    </Stack.Navigator>
);

class AppNavigators extends Component {


    render() {
        // {this.state.isInitialScreen && (<DoubleTapToClose message="Tap again to exit app" />)}
        return (

                <StackScreen />
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        tabButton: state.tabButton.data
    };
};

export default connect(mapStateToProps)(AppNavigators);
