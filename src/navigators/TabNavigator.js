import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
// import {createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
    TouchableOpacity,
    Text
} from 'react-native'
import colors from "../assets/styles/colors";
import House from "../assets/SVG/TabIcon/House";
import Saved from "../containers/Saved";
import Heart from "../assets/SVG/TabIcon/Heart";
import Components from "../components";
import Messages from "../containers/Messages";
import SMS from "../assets/SVG/TabIcon/SMS";
import Profile from "../containers/Profile";
import User from "../assets/SVG/TabIcon/User";
import Plus from 'assets/SVG/TabIcon/Plus'
import {get} from 'lodash'
import {connect} from "react-redux";
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

import HomeStack from 'navigators/HomeStack'
import NavigationService from "./NavigationService";
import {withTranslation} from "react-i18next";

let TabNav = (props) => (
    <Tab.Navigator
        initialRouteName="home"
        activeColor={colors.brandColor}
        inactiveColor={colors.second}
        barStyle={{backgroundColor: '#fff'}}
        tabBarOptions={{
            style: {
                paddingBottom: 15,
                // borderWidth:1,
                height: 65,
                paddingTop: 10
            },

        }}

    >
        <Tab.Screen name="home"
                    component={HomeStack}
                    options={({route}) => ({
                        tabBarLabel: props.t('Home'),
                        tabBarIcon: ({color, focused}) => (
                            <House color={color} size={26}/>
                        ),
                    })}
        />
        <Tab.Screen name="saved"
                    component={Saved}
                    options={{
                        tabBarLabel: props.t('Saved'),
                        tabBarIcon: ({color, focused}) => (
                            <Heart color={color} size={26}/>
                        ),
                    }}
                    listeners={({ navigation, route }) => ({
                        tabPress: e => {
                            // Prevent default action
                            e.preventDefault();
                            console.log("TOKEN:",get(props.user,'data.access_token','')),

                                get(props.user,'data.access_token','')?(
                                    navigation.navigate('saved')
                                ):(
                                    navigation.navigate('signIn')
                                )
                        },
                    })}
        />
        <Tab.Screen name="add"
                    component={() => null}
                    options={{
                        tabBarButton: () => {
                            return (
                                <Components.AddButton  background={colors.brandColor} icon={<Plus color={colors.white}/>}  touch={()=>{
                                    get(props.user,'data.access_token','')?(
                                        NavigationService.navigate('newlisting')
                                    ):(
                                        NavigationService.navigate('signIn')
                                    )

                                }}/>
                            )
                        }
                    }}


        />
        <Tab.Screen name="messages"
                    component={Messages}
                    options={{
                        tabBarLabel: props.t('Messages'),
                        tabBarIcon: ({color, focused}) => (
                            <SMS color={color} size={26}/>
                        ),
                    }}
                    listeners={({ navigation, route }) => ({
                        tabPress: e => {
                            // Prevent default action
                            e.preventDefault();
                            console.log("TOKEN:",get(props.user,'data.access_token','')),

                                get(props.user,'data.access_token','')?(
                                navigation.navigate('messages')
                                ):(
                                navigation.navigate('signIn')
                            )

                        },
                    })}
        />
        <Tab.Screen name="profile"
                    component={Profile}
                    options={{
                        tabBarLabel: props.t('Profile'),
                        tabBarIcon: ({color, focused}) => (
                            <User color={color} size={26}/>
                        ),
                    }}
                    listeners={({ navigation, route }) => ({
                        tabPress: e => {
                            // Prevent default action
                            e.preventDefault();
                            console.log("TOKEN:",get(props.user,'data.access_token','')),

                                get(props.user,'data.access_token','')?(
                                    navigation.navigate('profile')
                                ):(
                                    navigation.navigate('signIn')
                                )

                        },
                    })}
        />

    </Tab.Navigator>
)


const mapStateToProps = (state, ownProps) => {
    return {
        tabButton: state.tabButton.data,
        user:state.user.user,

    };
};

TabNav = connect(mapStateToProps)(TabNav)
export default withTranslation('main')(TabNav);
