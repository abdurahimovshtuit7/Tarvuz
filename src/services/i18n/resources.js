import {Text} from "react-native";
import React from "react";

export default {
    en: {
        main: {
            "Search": "Search",
            "All categories": "All categories",
            "Home":"Home",
            "Saved":"Saved",
            "Messages":"Messages",
            "Profile":"Profile",
            "Saved listings":"Saved listings",
            "My Active Listings":"My Active Listings",
            "My Archived Listings":"My Archived Listings",
            "Account Details":"Account Details",
            "Payments":"Payments",
            "Contact":"Contact",
            "Terms of use":"Terms of use",
            "Log Out":"Log Out",
            "Settings":"Settings",
            "Add Funds":"Add Funds",
            "Balance":"Balance",
            "Reactivate":"Reactivate",
            "Delete":"Delete",
            "All on The property":"All on The property",
            "Cancel":"Cancel",
            "Category":"Category",
            "New listing":"New listing",
            "Choose category":"Choose category"

        }
    },
    ru: {
        main: {
            "Search": "SEARCH",
            "All categories": "All",
            "Home":"Home",
            "Saved":"Saved",
            "Messages":"Messages",
            "Profile":"Profile",
            "Saved listings":"Saved listings",
            "My Active Listings":"My Active Listings",
            "My Archived Listings":"My Archived Listings",
            "Account Details":"Account Details",
            "Payments":"Payments",
            "Contact":"Contact",
            "Terms of use":"Terms of use",
            "Log Out":"Log Out",
            "Settings":"Settings",
            "Add Funds":"Add Funds",
            "Balance":"Balance",
            "Reactivate":"Reactivate",
            "Delete":"Delete",
            "All on The property":"All on The property"
        }
    },
    uz: {
        main: {
            "Search": "Qidirish",
            "All categories": " categories",
            "Home":"Asosiy",
            "Saved":"Saved",
            "Messages":"Messages",
            "Profile":"Profile",
            "Saved listings":"Saved listings",
            "My Active Listings":"My Active Listings",
            "My Archived Listings":"My Archived Listings",
            "Account Details":"Account Details",
            "Payments":"Payments",
            "Contact":"Contact",
            "Terms of use":"Terms of use",
            "Log Out":"Log Out",
            "Settings":"Settings",
            "Add Funds":"Add Funds",
            "Balance":"Balance",
            "Reactivate":"Reactivate",
            "Delete":"Delete",
            "All on The property":"All on The property"

        }
    }
};
