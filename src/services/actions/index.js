import {SET_LANGUAGE, TAB_BUTTON,SAVE_USER} from "../constants"

export const changeLanguage = lang => ({
    type:SET_LANGUAGE,
    lang,
});

export const setTab = data =>({
    type:TAB_BUTTON,
    data
})

export const saveUser = user => ({
    type:SAVE_USER,
    user,
})
