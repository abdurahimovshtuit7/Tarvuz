import { createRoutine, promisifyRoutine } from 'redux-saga-routines'

export const Login = createRoutine('LOGIN');
export const signUp = createRoutine('SIGN_UP');
export const resentPost = createRoutine('RESENT_POST');
export const confirm = createRoutine('CONFIRM')
export const facebookConfirm = createRoutine('FACEBOOK_CONFIRM')


export default {
    Login: promisifyRoutine(Login),
    signUp: promisifyRoutine(signUp),
    resentPost: promisifyRoutine(resentPost),
    confirm: promisifyRoutine(confirm),
    facebookConfirm: promisifyRoutine(facebookConfirm),

}
