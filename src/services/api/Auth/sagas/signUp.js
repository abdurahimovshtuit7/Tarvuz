import { call, put, takeEvery, all } from 'redux-saga/effects'

import { signUp } from '../routines'
import TokenStorage from '../../../TokenStorage';

function * trigger (api, action) {
    const { request } = action.payload;
    // console.warn(JSON.stringify(action.payload));
    try {
        yield put(signUp.request());

        const response = yield call(api.auth.signUp, request);
        console.warn(JSON.stringify(response));
        yield all([
            put(
                signUp.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        yield put(signUp.failure(e))
    } finally {
        yield put(signUp.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(signUp.TRIGGER, trigger, api)
}
