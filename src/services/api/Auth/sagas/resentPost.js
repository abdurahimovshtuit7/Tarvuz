import { call, put, takeEvery, all } from 'redux-saga/effects'

import { resentPost } from '../routines'
import TokenStorage from '../../../TokenStorage';

function * trigger (api, action) {
    const { request } = action.payload;
    // console.warn(JSON.stringify(action.payload));
    try {
        yield put(resentPost.request());

        const response = yield call(api.auth.resentPost, request);
        console.warn(JSON.stringify(response));
        yield all([
            put(
                resentPost.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        yield put(resentPost.failure(e))
    } finally {
        yield put(resentPost.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(resentPost.TRIGGER, trigger, api)
}
