import { call, put, takeEvery, all } from 'redux-saga/effects'

import { facebookConfirm } from '../routines'
import TokenStorage from '../../../TokenStorage';

function * trigger (api, action) {
    const { request } = action.payload;
    // console.warn(JSON.stringify(action.payload));
    try {
        yield put(facebookConfirm.request());

        const response = yield call(api.auth.facebookConfirm, request);
        console.warn(JSON.stringify(response));
        yield all([
            put(
                facebookConfirm.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        yield put(facebookConfirm.failure(e))
    } finally {
        yield put(facebookConfirm.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(facebookConfirm.TRIGGER, trigger, api)
}
