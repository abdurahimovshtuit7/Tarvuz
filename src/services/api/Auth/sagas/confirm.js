import { call, put, takeEvery, all } from 'redux-saga/effects'

import { confirm } from '../routines'
import TokenStorage from '../../../TokenStorage';

function * trigger (api, action) {
    const { request } = action.payload;
    // console.warn(JSON.stringify(action.payload));
    try {
        yield put(confirm.request());

        const response = yield call(api.auth.confirm, request);
        console.warn(JSON.stringify(response));
        yield all([
            put(
                confirm.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        yield put(confirm.failure(e))
    } finally {
        yield put(confirm.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(confirm.TRIGGER, trigger, api)
}
