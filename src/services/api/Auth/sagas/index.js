import { all } from "redux-saga/effects";

import Login from './Login'
import confirm from './confirm'
import resentPost from './resentPost'
import signUp from './signUp'
import facebookConfirm from "./facebookConfirm";


export default function * sagas (api) {
    yield all(
        [
            Login(api),
            signUp(api),
            resentPost(api),
            confirm(api),
            facebookConfirm(api)
        ]
    )
}
