import configs from '../config'

const staticsBaseURL = 'http://example.com';


export default {
  staticsBaseURL,

  apisauce: {
    baseURL: configs.API_ROOT,

    headers: {
      'Accept': 'application/json',
      // 'Cache-Control': 'no-cache',
      'Content-Type': 'application/json',
      'X-AUTH':'Basic YW5kcm9pZDphbmRyb2lk',
      'User-Agent':'PostmanRuntime/7.26.2',
      // 'Host':`<calculated when request is sent>`
    }
  }
}
