import auth from './Auth/routines'
import ads from './Ads/routines'

export default {
    auth,
    ads
}
