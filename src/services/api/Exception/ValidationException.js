import Exception from './Exception'

class ValidationException extends Exception {
  constructor (errors, message, id,request) {
    super(message, id,request)
    // console.warn('Errors ', JSON.stringify(errors))
    this.errors = errors
  }
}

export default ValidationException
