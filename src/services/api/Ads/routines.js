import { createRoutine, promisifyRoutine } from 'redux-saga-routines'
//
// export const approvePhone = createRoutine('APPROVE_PHONE_NUMBER');
export const getItems = createRoutine('GET_ITEMS');
export const getCategories = createRoutine('GET_CATEGORIES');
export const addFavorite = createRoutine('ADD_FAVORITE')
export const favoritesList = createRoutine('FAVORITE_LIST')
// export const getSettings = createRoutine('GET_SETTINGS')

export default {
//     approvePhone: promisifyRoutine(approvePhone),
       getItems: promisifyRoutine(getItems),
       getCategories: promisifyRoutine(getCategories),
       addFavorite: promisifyRoutine(addFavorite),
       favoritesList: promisifyRoutine(favoritesList),
//     getSettings:promisifyRoutine(getSettings)
}
