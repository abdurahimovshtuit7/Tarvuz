import { SET_LANGUAGE } from '../constants'

const initialLanguage = {
    lang:'en',
};

export default (state = initialLanguage, action) => {
    switch (action.type) {
        case SET_LANGUAGE:{
            return {
                ...state,
                lang: action.lang
            };
        }
        default:
            return state;
    }
}
