import {
    getCategories,
    getItems,
    addFavorite,
    favoritesList
} from '../api/Ads/routines'
import { SET_LANGUAGE, LOGOUT } from '../constants'
import TokenStorage from '../TokenStorage'
import get from 'lodash/get'


const initial = {
    items: {
        data:[],
        links: {}
    },
    categories: {},
    addFavorite:{},
    Favorite:{},
    favoritesList:{

    }
};

export default (state = initial, action) => {
    // let array = {}
    // get(state.Favorite,'data',[]).map((item,index)=>{
    //     array = {
    //         ...array,
    //         [item.id]:item
    //     }
    // })
    // state.addFavorite = {
    //     ...array
    // }
    switch (action.type){
        case getItems.SUCCESS:{
            let olderData = [];
            let data = get(action, 'payload.response.data.data', []);
            let links = get(action,'payload.response.data.links',{});
            let {page} = get(action,'payload.request')

            if(page !==1)
                olderData = get(state,'items.data',[])
            return {
                ...state,
                items:{
                    data:[...olderData,...data],
                    links
                }
            }
        }
        case getCategories.SUCCESS:{
            let data = get(action, 'payload.response.data', {});

            return {
                ...state,
                categories: data
            }
        }
        case addFavorite.REQUEST:{
            let data = get(action, 'payload.data.item', {});
            let id = get(action,'payload.data.id','')
            let obj = {...state.favoritesList}

            if(obj[id]){
                delete obj[id]
            } else {
                obj[id]=data
            }

            return {
                ...state,
                favoritesList: {
                    ...obj,
                    // [id]:data,
                }
            }
        }
        case addFavorite.SUCCESS:{
            return {
                ...state,
                // addFavorite: data
            }
        }
        case addFavorite.FAILURE:{
            let data = get(action, 'payload.request.data.item', {});
            let id = get(action,'payload.request.data.id','')
            let obj = {...state.favoritesList}

            if(!obj[id]){
                obj[id]=data
            } else {
                delete obj[id]
            }

            return {
                ...state,
                favoritesList: {
                    ...obj,
                    // [id]:data,
                }
            }
        }
        case favoritesList.SUCCESS:{
            let data = get(action, 'payload.response.data', {});
            let array = {}
            get(state.Favorite,'data',[]).map((item,index)=>{
                array = {
                    ...array,
                    [item.id]:item
                }
            })
            return {
                ...state,
                Favorite:data,
                favoritesList: {
                    ...array,
                    ...state.favoritesList,
                }

            }
        }

    }
    return state
}
