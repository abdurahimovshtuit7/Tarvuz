import { TAB_BUTTON } from '../constants'


const initial = {
    data:{
        tab:'1',
        focused:true
    },
};

export default (state = initial, action) => {
    switch (action.type) {
        case TAB_BUTTON:{
            return {...state, data: action.data};
        }
        default:
            return state;
    }
}
