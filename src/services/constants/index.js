const NAME = 'TARVUZ';
export const LOGOUT = `${NAME}/LOGOUT`;
export const SET_LANGUAGE = `${NAME}/SET_LANGUAGE`;
export const TAB_BUTTON = `${NAME}/TAB_BUTTON`
export const SAVE_USER = `${NAME}/SAVE_USER`

