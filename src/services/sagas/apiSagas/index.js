import Auth from '../../api/Auth/sagas'
import Ads from '../../api/Ads/sagas'

export default function sagas (api) {
    return [
        Auth(api),
        Ads(api)
    ]
}
