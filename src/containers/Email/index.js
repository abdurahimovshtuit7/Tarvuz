import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from "react-native";
import styles from './styles'
import colors from "../../assets/styles/colors";
import {Field,withFormik} from "formik";
import Components from '../../components'
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import * as Yup from "yup";
import {Routines} from "../../services/api";
import SignUp from "../SignUp";
import {connect} from "react-redux";

class Email extends Component {
    render() {
        const {navigation,handleSubmit} = this.props
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" enableOnAndroid={false} contentContainerStyle={styles.wrapper}>
                    <Components.Layout style={{flex: 1}}>
                        <Text style={styles.welcome}>
                            Create new account
                        </Text>


                        <Field
                            name={'first_name'}
                            label={'First name'}
                            placeHolder={'Username'}
                            component={Components.Input}
                        />
                        <Field
                            name={'last_name'}
                            label={'First name'}
                            placeHolder={'Lastname'}
                            component={Components.Input}
                        />
                        <Field
                            name={'email'}
                            // label={'First name'}
                            placeHolder={'Email'}
                            // mask={"+1 ([000]) [000] [00] [00]"}
                            keyboards={"phone-pad"}
                            component={Components.Input}
                        />
                        <Field
                            name={'password'}
                            label={'First name'}
                            placeHolder={'Password'}
                            component={Components.Input}
                        />

                        {/*<Components.Input placeHolder={'Username'}/>*/}
                        {/*<Components.Input placeHolder={'Phone number'}/>*/}
                        <Components.SignButton text={'Create account'} margin={"4%"} touch={()=>{handleSubmit()}}/>

                        {/*<Components.SignButton text={'Create account'} margin={"4%"} touch={()=>{navigation.navigate('verifyEmail')}}/>*/}


                    <TouchableOpacity onPress={() => {
                    }}>
                        <Text style={styles.bottom}>
                            By creating an account you agree with our
                            <Text style={styles.inline}> Terms of use</Text>
                        </Text>
                    </TouchableOpacity>
                    </Components.Layout>
                </KeyboardAwareScrollView>
            </SafeAreaView>
        )
    }
}
Email = withFormik({
    mapPropsToValues: () => ({
        first_name: "",
        last_name:"",
        email:"",
        password:""
    }),
    validationSchema: ({ t }) => {
        return Yup.object().shape({
            first_name: Yup.string().required("required"),
            // .matches(/^[a-z]+^[A-Z]+$/ , 'Is not in correct format'),
            last_name:Yup.string().required("required"),
                // .matches(/^[a-z]+$/ , 'Is not in correct format'),
            email:Yup.string().required("required")
                .email()
            ,
            password:Yup.string()
                .min(6, 'Belgilar soni kam!')
                .max(16, 'Too Long!')
                .required('required'),
        });
    },
    handleSubmit: (values, { props, setSubmitting})=>{
        console.log(values);

        let {first_name,last_name, email,password} = values;

        // let onlyNums = phone.replace(/[^\d]/g, '');
        // console.log("PHONE: ", onlyNums)
        setSubmitting(true);
        Routines.auth.signUp({
            request: {
                data: {
                    first_name: first_name,
                    last_name: last_name,
                    email: email,
                    password: password,
                    // phone:'+1'
                }
            }}, props.dispatch)
            .then(() => {
                // setSubmitting(false);
                // NavigationService.navigate('codeEntry', {
                //     phone,
                //     countryCode: code
                // })
            })
            .catch(e => {
                setSubmitting(false);
                if(e.message === "NETWORK_ERROR"){
                }
            });
    }
})(Email);
const mapStateToProps = (state, ownProps) => {
    return {

    };
};

export default connect(mapStateToProps)(Email)

