import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    ScrollView, Dimensions
} from "react-native";
import styles from './styles'
import colors from "../../assets/styles/colors";
import Components from '../../components'
import {get,_} from 'lodash'
import moment from "moment";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";

//----------------SVG---------------------
import {Routines} from "../../services/api";
import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
const windowWidth = Dimensions.get('window').width;
class Saved extends Component {
    state={

    }
    componentDidMount() {
        this.favoritesList()
    }

    favoritesList() {
        Routines.ads.favoritesList({
            request: {}
        }, this.props.dispatch)
            .then((data) => {
                    // console.log("e", payload) bu funksiya api 200 qaytsa ishlidi va payload qaytadi agar siz redux ga saqlashni xoxlamasangiz shu component state ga yozib ishlatshingiz mumkin
                },
            )
    }
    addFavorite(id,item) {
        Routines.ads.addFavorite({
            request: {
                data:{
                    id,
                    item
                }
            }
        }, this.props.dispatch)
            .then((data) => {

                },
            )
    }
    render() {
        const {navigation,favorites,t} = this.props
        let date = new Date().getDate()
        console.log("DATE:",date)
        console.log("FAV:",favorites)
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <View style={styles.component}>
                    <Text style={styles.text}>{moment().format('dddd D MMM')}</Text>
                    <Text style={styles.savedText}>{t('Saved listings')}</Text>
                </View>
                <ScrollView>
                    <View style={styles.view}>
                        {
                            _.toArray(get(this.props,'favoritesList',[])).map((item,index)=>{
                               return(
                                   <Components.Item
                                       key={index}
                                       item={item}
                                       savedId={true}
                                       saved={(id,item)=>{this.addFavorite(id,item)}}
                                       width={windowWidth/2.2}
                                       list = {get(this.props,'favoritesList',[])}
                                   />
                               )
                            })
                        }

                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        items: state.ads.items,
        favorites:state.ads.Favorite,
        favoritesList:state.ads.favoritesList,
    };
};
Saved = connect(mapStateToProps)(Saved)
export default withTranslation('main')(Saved)
