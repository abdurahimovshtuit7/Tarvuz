import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor:colors.white,
    },
    scroll:{
        marginLeft:0,
        // borderWidth:1,
    },
    component:{
        paddingHorizontal:15,
        paddingVertical:10,
    },

    view:{
        flex:1,
        flexDirection: 'row',
        justifyContent:'space-between',
        // paddingLeft:10,
        marginHorizontal: 10,
        // borderWidth:1,
        // flexGrow:1
        flexGrow:1,
        flexWrap:'wrap'
    },
    text:{
        fontSize: 12,
        color:colors.textGray,
        fontWeight:'600',
        paddingTop:6,
        textTransform: 'uppercase'
    },
    savedText:{
         fontSize:30,
        fontWeight: 'bold'
    }

})

export default styles
