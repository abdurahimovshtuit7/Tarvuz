import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from "react-native";
import styles from './styles'
import colors from "../../assets/styles/colors";
import Components from '../../components'
import Edit from '../../assets/SVG/Edit'
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";

//----------------SVG------------
import Promote from '../../assets/SVG/Profile/Promote'
import Archive from '../../assets/SVG/Profile/Archive'
import Reactivate from '../../assets/SVG/Profile/Reactivate'
import Delete from '../../assets/SVG/Profile/Delete'
import NavigationService from "../../navigators/NavigationService";

class ActiveListing extends Component {
    state = {

    }

    render() {
        const {navigation} = this.props
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <ScrollView>
                <Components.ArchiveButton title={'Apartment for rent, Du...'}
                                          item = {'Active: May 3 - May 22'}
                                          balance={'$ 2 449'}
                                          icon1={<Promote/>}
                                          icon2={<Archive/>}
                                          text1={'Promote'}
                                          text2={'Archive'}
                                          color={colors.second}
                                          touch={()=>{
                                              NavigationService.navigate('promotion')
                                          }}
                />
                    <Components.ArchiveButton title={'2bd, Flatbush Ave'}
                                              item = {'Active: May 3 - May 22'}
                                              balance={'$ 1 293'}
                                              icon1={<Promote/>}
                                              icon2={<Archive/>}
                                              text1={'Promote'}
                                              text2={'Archive'}
                                              color={colors.second}
                                              touch={()=>{
                                                  NavigationService.navigate('promotion')
                                              }}
                    />

                </ScrollView>
            </SafeAreaView>
        )
    }
}

export default ActiveListing
