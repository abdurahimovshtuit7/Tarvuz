import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from "react-native";
import styles from './styles'
import colors from "../../assets/styles/colors";
import Components from '../../components'
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";

class VerifyEmail extends Component {
    render() {
        const {navigation} = this.props
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" enableOnAndroid={false} contentContainerStyle={styles.wrapper}>
                    <Components.Layout style={{flex: 1}}>
                        <Text style={styles.welcome}>
                            Verify Email
                        </Text>
                        <Text style={styles.atribut}>
                            We sent a 5-digit code to abc@gmail.com
                        </Text>
                        <Text  style={styles.enterText}>
                            PLEASE ENTER VERIFICATION CODE
                        </Text>
                        {/*<Components.Input placeHolder={'Username'}/>*/}
                        {/*<Components.Input placeHolder={'Email'}/>*/}
                        {/*<Components.Input placeHolder={'Phone number'}/>*/}

                        {/*<Components.SignButton text={'Create account'} margin={"4%"}/>*/}
                        <Components.Verify/>
                    </Components.Layout>
                    <TouchableOpacity style={styles.button}
                                      onPress={() => {
                                          navigation.navigate('login')
                                      }}>
                        <Text style={styles.bottom}>
                            Re-send verification code
                        </Text>
                    </TouchableOpacity>

                </KeyboardAwareScrollView>
            </SafeAreaView>
        )
    }
}

export default VerifyEmail
