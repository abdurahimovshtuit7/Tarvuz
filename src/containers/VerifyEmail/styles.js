import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor:colors.white,
    },
    wrapper: {
        flex:1,
        flexGrow:1,
        paddingVertical: 36
    },
    welcome:{
        textAlign:'center',
        fontSize:30,
        fontWeight:'bold',
        color:colors.primary,
        // marginBottom: '10%'
    },
    bottom:{
        textAlign:'center',
        color:colors.brandColor,
        fontSize:16,
        fontWeight:'600',
    },
    inline:{
        fontWeight:'bold'
    },
    atribut:{
        fontSize:12,
        fontWeight:'600',
        color:colors.textGray,
        textAlign:'center',
        marginTop: 10,
        letterSpacing: 0.1,
        marginBottom:'20%'
    },
    enterText:{
        fontSize:12,
        fontWeight:'600',
        color:colors.textGray,
        textAlign:'center',
        // marginTop: 10,
        letterSpacing: 0.1,
        marginBottom: '5%'

    },
    button:{
        marginTop:"60%",
        marginHorizontal:50
    },


})

export default styles
