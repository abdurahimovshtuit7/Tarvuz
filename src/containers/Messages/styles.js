import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    scroll: {
        marginLeft: 0,
        // borderWidth:1,
    },
    component: {
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    text: {
        fontSize: 12,
        color: colors.textGray,
        fontWeight: '600',
        paddingTop: 6,
        textTransform: 'uppercase'
    },
    title: {
        fontSize: 30,
        fontWeight: 'bold'
    }


})

export default styles
