import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from "react-native";
import styles from './styles'
import colors from "../../assets/styles/colors";
import Components from '../../components'
import moment from "moment";
import MapView from "react-native-maps";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";

//----------------SVG---------------------
import All from '../../assets/SVG/Categories/All'
import House from '../../assets/SVG/Categories/House'
import Car from '../../assets/SVG/Categories/Car'
import Lamp from '../../assets/SVG/Categories/Lamp'
import Animal from '../../assets/SVG/Categories/Animal'
import Bike from '../../assets/SVG/Categories/Bike'
import Work from '../../assets/SVG/Categories/Work'
import Search from '../../assets/SVG/Search'
import NavigationService from "../../navigators/NavigationService";
import {connect} from "react-redux";
import {withTranslation} from "react-i18next";

import {TriggeringView} from "react-native-image-header-scroll-view";

class Message extends Component {
    state={
        region: {
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        },
    }
    getInitialState() {
        return {
            region: {
                latitude: 37.78825,
                longitude: -122.4324,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            },
        };
    }

    onRegionChange(region) {
        this.setState({ region });
    }
    render() {
        const {navigation,t} = this.props
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <View style={styles.component}>
                    <Text style={styles.text}>{moment().format('dddd D MMM')}</Text>
                    <Text style={styles.title}>{t('Messages')}</Text>
                </View>
                <ScrollView>
                 <Components.MessageButton touch={()=>{
                     NavigationService.navigate('chat')
                 }}/>
                 <Components.MessageButton touch={()=>{
                     NavigationService.navigate('chat')
                 }}/>
                    <MapView
                        region={this.state.region}
                        onRegionChange={this.onRegionChange}
                    />
                </ScrollView>
            </SafeAreaView>
        )
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        items: state.ads.items,
        favorites:state.ads.Favorite
    };
};
Message = connect(mapStateToProps)(Message)
export default withTranslation('main')(Message)
