import {StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.view,
    },
    scroll: {
        marginLeft: 0,
        // borderWidth:1,
    },
    component: {
        paddingHorizontal: 15,
        paddingVertical: 15,
        // borderWidth:1,
        justifyContent: 'space-between',
        flexDirection:'row',
        alignItems: 'center',
        backgroundColor:colors.white

    },
    row:{
        flexDirection:'row',
        // borderWidth:1,
        alignItems: 'center',

    },
    rows:{
        marginLeft:10,
        flexDirection:'row',
        // borderWidth:1,
        alignItems: 'center',
        width:'68%',
    },
    icon:{
        // position: "absolute",
        // right: 0,
        // top:0,
        // borderWidth:3,
        width:37,
        height:37,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:20,
        borderColor:'white',
        // paddingLeft:10

    },
    iconText:{
        fontSize:12
    },
    view:{
        paddingLeft: 10,

        // borderWidth:1
    },
    name:{
        fontSize: 16,
        color:colors.primary,
    },
    status:{
        fontSize:12,
        color:colors.green,
    },
    call:{
        // width: '10%',
        // borderWidth:1,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:20,
        paddingHorizontal:15,
        paddingVertical: 10,
        // marginVertical:10,
    },
    rent: {
        // flex: 1,
        flexDirection: 'row',
        borderWidth: 1,
        // marginHorizontal: 15,
        padding: 15,
        // borderRadius: 10,
        borderColor: colors.border,
        backgroundColor: colors.white,
        // elevation: 7,
        // marginVertical:10,
    },
    image: {
        width: 40,
        height: 40,
        borderRadius: 5,
        overflow: 'hidden'
    },
    div: {
        width: '80%',
        // borderWidth: 1,
        paddingLeft: 15
    },
    text: {
        fontSize: 14,
        color: colors.primary
    },
    sum: {
        fontSize: 14,
        color: colors.second
    },


    view1: {
        flex: 1,
        // marginTop: 70,
        zIndex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: colors.background,
        //paddingTop:8
    },

    text1: {
        // borderWidth: 1,
        color: 'white',
        fontSize: 22,
        paddingLeft: 10,
        fontFamily: 'Ubuntu',
    },
    button: {
        //borderWidth:1,
        marginRight: 20,
        backgroundColor: colors.white,
        paddingHorizontal: 8,
        paddingVertical: 8,
        borderRadius: 20,
    },
    timeRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 90,
    },
    string: {
        fontSize: 18,
        borderBottomWidth: 0.5,
        marginBottom: 19,
        color: '#464646',
        fontFamily: 'Ubuntu',
    },
    chat: {
        zIndex: 1,
    },
    send: {
        backgroundColor: colors.brandColor,
        height: 36,
        marginBottom:5,
        justifyContent: 'center',
        width: 36,
        alignItems: 'center',
        borderRadius:40,
        // borderBottomRightRadius: 5,
        // borderTopRightRadius: 5,
        elevation: 3,

    },
    textInput:{
        borderWidth: 1,
        marginHorizontal:10,
        borderRadius:20,
        marginVertical:5,
        paddingHorizontal:14,
        borderColor:colors.border
    },
    attach:{
        marginBottom:10,
        paddingHorizontal:4,
        justifyContent: 'center',
    }



})

export default styles
