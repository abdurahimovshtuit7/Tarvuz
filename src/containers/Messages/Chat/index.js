import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from "react-native";
import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components'
import Ionicons from 'react-native-vector-icons/MaterialIcons'
import Icon from 'react-native-vector-icons/Ionicons'

import {GiftedChat, InputToolbar, Bubble, Send,Time} from 'react-native-gifted-chat';
// import ImagePicker from 'react-native-image-picker';


//----------------SVG---------------------
import Call from 'assets/SVG/Call'
import ArrowUp from 'assets/SVG/ArrowUp'
import Attach from 'assets/SVG/Attach'


class Chat extends Component {
    state={
        messages: [],
        avatarSource:null
    }
    componentWillMount() {
        this.setState({
            messages: [
                {
                    _id: 1,
                    text: 'Hello developer',
                    createdAt: new Date(),
                    user: {
                        _id: 2,
                        name: 'React Native',
                        avatar: 'https://placeimg.com/140/140/any',
                    },
                    sent: true,
                    // Mark the message as received, using two tick
                    received: true,
                },
            ],
        })
    }
    handleAddPicture = () => {
        // const options = {
        //     title: 'Select Avatar',
        //     customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
        //     storageOptions: {
        //         skipBackup: true,
        //         path: 'images',
        //     },
        // };        ImagePicker.showImagePicker(options, (response) => {
        //     console.log('Response = ', response);
        //
        //     if (response.didCancel) {
        //         console.log('Auth cancelled image picker');
        //     } else if (response.error) {
        //         console.log('ImagePicker Error: ', response.error);
        //     } else if (response.customButton) {
        //         console.log('Auth tapped custom button: ', response.customButton);
        //     } else {
        //         const source = { uri: response.uri };
        //
        //         // You can also display the image using data:
        //         // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        //
        //         this.setState({
        //             avatarSource: source,
        //         });
        //     }
        // });
//         ImagePicker.launchCamera(options, (response) => {
//             // Same code as in above section!
//         });
//
// // Open Image Library:
//         ImagePicker.launchImageLibrary(options, (response) => {
//             // Same code as in above section!
//         });
    };
    customtInputToolbar = props => {
        return (
            <InputToolbar
                {...props}
                containerStyle={{
                    // borderRadius: 5,
                    // marginHorizontal: 9,
                    // marginBottom: 10,
                    // elevation: 4,
                    paddingHorizontal:15,
                    paddingVertical:10,
                    borderWidth:1,
                    justifyContent:'center',
                    // alignItems:'center'

                }}
            >
                <TouchableOpacity>

                </TouchableOpacity>
            </InputToolbar>
        );
    };
    renderActions = props => {
        return (
            <TouchableOpacity style={styles.attach} onPress={()=>{
                this.handleAddPicture()
            }}>
              <Attach/>
            </TouchableOpacity>
        )
    }
    renderBubble = props => {
        return (
            <Bubble
                {...props}
                textStyle={{
                    right: {
                        paddingVertical: 8,
                        color:colors.white,
                    },
                    left: {
                        paddingVertical: 8,
                    },
                }}
                wrapperStyle={{
                    left: {
                        backgroundColor: 'white',
                        elevation: 4,
                        marginBottom: 15,
                        marginLeft:10
                    },
                    right: {
                        backgroundColor: colors.brandColor,
                        elevation: 4,
                        marginBottom: 15,
                        marginRight:10

                    },
                }}
            />
        )
    };


    renderTime = props => {
        return (
            <Time {...props}
                  textStyle={{
                      right: {
                          color:colors.white ,
                      },
                      left: {
                          color:'#000' ,
                      }
                  }}
            />
        );
    }
    onSend(messages = []) {
        this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, messages),
        }))
    }

    renderSend = (sendProps) => {
        if (sendProps.text.trim().length > 0) {
            return (
                <Send {...sendProps} containerStyle= {styles.send}>
                    <View>
                        <ArrowUp/>
                    </View>
                </Send>
            );
        }
        return null;
    };

    render() {
        const {navigation} = this.props
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <View style={styles.component}>
                  <View style={styles.row}>
                      <Ionicons name="arrow-back" size={25} style={{width:35}} onPress={() => {
                          navigation.goBack()
                      }}/>
                      <View style={styles.rows}>
                          <Components.UserImage
                              style={styles.icon}
                              // uri={ require('../../assets/images/Ellipse.png')}
                              username={"Sobir Rahimov"}
                              textStyle={styles.iconText}
                          />
                         <View style={styles.view}>
                          <Text style={styles.name} numberOfLines={1}>
                              Aleksey Petrov
                          </Text>
                             <Text style={styles.status} numberOfLines={1}>Online</Text>
                         </View>
                      </View>

                  </View>
                    <TouchableOpacity style={styles.call}>
                    <Call/>
                    </TouchableOpacity>
                </View>
                <View style={styles.rent}>
                    <Image source={require('assets/example/img1.png')} style={styles.image}/>
                    <View style={styles.div}>
                        <Text style={styles.text} numberOfLines={1}>
                            Roommate needed, Coney Island
                        </Text>
                        <Text style={styles.sum} numberOfLines={1}>
                            $ 480
                        </Text>
                    </View>
                </View>
                <View style={styles.view1}>
                    <GiftedChat
                        style={styles.chat}
                        messages={this.state.messages}
                        onSend={messages => this.onSend(messages)}

                        user={{
                            _id: 0,
                        }}
                        // locale={'ru'}
                        placeholder={'Message'}
                        isAnimated={true}
                        renderInputToolbar={props => this.customtInputToolbar(props)}
                        renderBubble={this.renderBubble}
                        renderSend={props => this.renderSend(props)}
                        showUserAvatar={false}
                        renderAvatar={null}
                        // renderComposer={this.renderComposer}
                        bottomOffset={-10}
                        isTyping={true}
                        // messageIdGenerator={this.messageIdGenerator}
                        textInputStyle={styles.textInput}
                        renderTime={this.renderTime}
                        timeFormat={'LT'}
                        renderActions={props => this.renderActions(props)}
                        scrollToBottom
                        scrollToBottomComponent={() => (
                            <Ionicons name='keyboard-arrow-down' size={34} color='#000' />
                        )}
                        // maxInputLength={3}
                    />
                </View>
            </SafeAreaView>
        )
    }
}

export default Chat
