import React, {Component} from 'react';
import {
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    Image,
    ImageBackground,
    SafeAreaView,
    StatusBar, Dimensions
} from 'react-native';
import Ionicons from 'react-native-vector-icons/MaterialIcons'
import {get} from 'lodash'
import Swiper from "react-native-swiper";
import HeaderImageScrollView, {TriggeringView} from 'react-native-image-header-scroll-view';
import LinearGradient from 'react-native-linear-gradient';
import colors from "assets/styles/colors";
import styles from "./styles";
import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import {Routines} from "services/api";
import Components from "components";
import NavigationService from "navigators/NavigationService";
import MapView from 'react-native-maps';

//----------SVG-------------
import Share from 'assets/SVG/Share'
import SaveIcon from 'assets/SVG/Save'
import Warning from 'assets/SVG/Warning'

const windowWidth = Dimensions.get('window').width;

class SingleListing extends Component {
    state = {
        refreshing: false,
    }

    addFavorite(id, item) {
        Routines.ads.addFavorite({
            request: {
                data: {
                    id,
                    item
                }
            }
        }, this.props.dispatch)
            .then((data) => {

                },
            )
    }

    render() {
        const {navigation, route, items} = this.props
        console.log("ITEMS:", items)
        const data = get(this.props.route.params, 'item', {})
        return (
            <SafeAreaView style={styles.container}>
                <HeaderImageScrollView
                    maxHeight={260}
                    minHeight={60}
                    disableHeaderGrow={true}
                    minOverlayOpacity={0}
                    maxOverlayOpacity={0}
                    bounces={true}
                    // useNativeDriver={true}
                    // headerImage={require("../../assets/Artboard.png")}
                    // refreshControl={
                    //     <RefreshControl
                    //         refreshing={this.state.refreshing}
                    //         onRefresh={this._onRefresh.bind(this)}
                    //         tintColor="white"
                    //     />
                    // }
                    // renderTouchableFixedForeground={()=>(
                    //     <View>
                    //         <Ionicons name="arrow-back" size={25} style={{marginLeft: 20,marginTop:20,position:'absolute'}} onPress={() => {
                    //             navigation.goBack()
                    //         }}/>
                    //     </View>
                    // )}
                    fadeOutForeground={true}
                    foregroundParallaxRatio={1}
                    renderTouchableFixedForeground={() => (
                        <View style={styles.containerS}>
                            <Ionicons name="arrow-back" size={25}
                                      style={{marginLeft: 20, marginTop: 20, position: 'absolute', zIndex: 1}}
                                      color={colors.white} onPress={() => {
                                navigation.goBack()
                            }}/>
                            {/*<LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={styles.linearGradient}>*/}

                            <View style={styles.containerS}>
                                <Swiper style={styles.wrapper}
                                        dot={<View style={styles.dotStyle}/>}
                                        activeDot={<View style={styles.activeDotStyle}/>}
                                        paginationStyle={styles.Pagination}
                                        loop={true}
                                >
                                    <View style={styles.slide1}>
                                        <ImageBackground source={require('../../assets/example/AnyConv.com__1.png')}
                                                         style={styles.images}>
                                            <LinearGradient colors={['rgba(0,0,0,0.8)', 'rgba(0, 0, 0, 0.1)']}
                                                            style={styles.wrapper}/>
                                            <View style={[styles.box, {backgroundColor: `rgba(0, 0, 0, 0.1)`}]}>
                                                <Text style={styles.text2}>
                                                    Jun 1, 22:12
                                                </Text>
                                            </View>
                                        </ImageBackground>
                                    </View>
                                    <View style={styles.slide2}>
                                        <ImageBackground source={require('../../assets/example/AnyConv.com__3.png')}
                                                         style={styles.images}>
                                            <LinearGradient colors={['rgba(0,0,0,0.8)', 'rgba(0, 0, 0, 0.1)']}
                                                            style={styles.wrapper}/>
                                            <View style={[styles.box, {backgroundColor: `rgba(0, 0, 0, 0.1)`}]}>
                                                <Text style={styles.text2}>
                                                    Jun 1, 22:12
                                                </Text>
                                            </View>
                                        </ImageBackground>
                                    </View>

                                </Swiper>

                                <Share style={{right: 60, top: 20, position: 'absolute', zIndex: 1}}
                                       color={colors.white}/>
                                <SaveIcon style={{right: 15, top: 22, position: 'absolute', zIndex: 1}}
                                          color={colors.white}/>
                            </View>
                            {/*</LinearGradient>*/}

                        </View>
                    )}
                >
                    <ScrollView style={styles.scroll}>

                        <TriggeringView onHide={() => console.log("text hidden")}>
                            <Text style={styles.text}>
                                $ 244 940
                            </Text>
                            <Text style={styles.text1}>
                                3bd, For rent, Park Avenue
                            </Text>
                            <Components.SendMessage/>
                            <View style={styles.wrap}>
                                <Components.Frame text={data.type}/>
                                <Components.Frame text={'Number of bedroms:'} number={'3'}/>
                                <Components.Frame text={'Funished'} number={'Yes'}/>
                                <Components.Frame text={'Number of floors:'} number={'6'}/>
                                <Components.Frame text={'Floor:'} number={'3'}/>
                                <Components.Frame text={'Size:'} number={'1400 Sqft'}/>
                            </View>
                            <Text style={styles.description}>
                                {data.description}
                            </Text>
                            //----------MAP----------------

                            <View style={styles.button}>
                                <Components.Owner name={'Ivan Petrov'} listings={'More listings: 8'} status={'online'}
                                                  touch={() => {
                                                      NavigationService.navigate('owner', {title: "Listing Owner"});
                                                  }}/>

                            </View>

                            <Text style={styles.title}>
                                More listings from the user
                            </Text>
                            <ScrollView horizontal={true} contentContainerStyle={styles.horizontal}
                                        showsHorizontalScrollIndicator={false}
                            >
                                {/*{*/}
                                {/*    get(items, 'data', []).map((item, index) => {*/}
                                {/*        return (*/}
                                {/*            <View style={styles.items}>*/}
                                {/*                <Components.Item*/}
                                {/*                    key={item.id}*/}
                                {/*                    index={index}*/}
                                {/*                    width={windowWidth / 2.17}*/}
                                {/*                    item={item}*/}
                                {/*                    saved={(id, item) => {*/}
                                {/*                        this.addFavorite(id, item)*/}
                                {/*                    }}*/}
                                {/*                    list={get(this.props, 'favoritesList', {})}*/}
                                {/*                />*/}
                                {/*            </View>*/}
                                {/*        )*/}
                                {/*    })*/}
                                {/*}*/}

                            </ScrollView>
                            <View style={styles.footer}>
                                <View style={styles.footerRow}>
                                    <Text style={styles.footerText}>
                                        ID: 8234
                                    </Text>
                                    <Text style={styles.footerText}>
                                        Views: 932
                                    </Text>
                                </View>
                                <View style={styles.footerWrap}>
                                    <TouchableOpacity style={styles.footerB}>
                                        <Warning/>
                                        <Text style={styles.buttonT}>
                                            Report this listing
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </TriggeringView>

                    </ScrollView>
                </HeaderImageScrollView>
            </SafeAreaView>
        );
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
        items: state.ads.items,
        categories: state.ads.categories,
        currentLangCode: state.language.lang,
        favoritesList: state.ads.favoritesList,
        favorites: state.ads.Favorite
    };
};

SingleListing = connect(mapStateToProps)(SingleListing)
export default withTranslation('main')(SingleListing)
