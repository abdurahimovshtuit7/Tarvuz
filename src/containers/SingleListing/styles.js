import {StyleSheet, Platform, Dimensions} from 'react-native';
import colors from 'assets/styles/colors'
import {horizontal} from "assets/styles/common";
var {height, width} = Dimensions.get('window');


const styles = StyleSheet.create({
    container: {
        flex: 1,
        // marginVertical:10,
        backgroundColor:colors.white,
        // borderWidth:1,
        // justifyContent:'center',
    },
    linearGradient: {
        flex: 1,
    },

    view:{
        flex:1,
        // flex: 0.48,
        flexDirection: 'row',
        justifyContent:'space-between',
        // marginHorizontal: 9,
        flexWrap:'wrap'
    },
    row:{
        flexDirection:'row',
        // flex:1
        justifyContent: 'space-between',
        paddingHorizontal:18,
        paddingTop:16,
        paddingBottom:10
    },
    text:{
        fontSize:24,
        marginTop:20,
        fontWeight:'bold',
        color:colors.primary,
        paddingHorizontal: 15,
        // textAlign:'center'
    },
    text1:{
       fontSize: 12,
       marginVertical:10,
        color:colors.primary,
        paddingHorizontal: 15,
    } ,

    containerS:{
        // borderWidth:1,
        // marginTop:13,
        flex:1,


    },
    wrapper: {
        height: "100%",
        //borderRadius:10
        // paddingBottom:10
        // backgroundColor:'rgba(255, 0, 255, 1.0)'

    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor:'rgba(255, 0, 255, .3)'
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor:'rgba(255, 0, 255, .3)'

    },

    dotStyle:{
        backgroundColor: 'rgba(255,255,255,.3)',
        width: 7,
        height: 7,
        borderRadius: 6,
        marginLeft: 3,
        marginRight: 3,
        borderColor:'white',
        borderWidth:1
    },
    activeDotStyle:{
        backgroundColor: '#fff',
        width: 7,
        height: 7,
        borderRadius: 6,
        marginLeft: 3,
        marginRight: 3,
    },
    Pagination:{
        bottom:12,

    },
    images:{
        width:width,
        height:"100%",
        // borderRadius: 15,
        // overflow: "hidden",
        // backgroundColor:'rgba(255, 0, 255, .3)'

    },
    scroll:{
        // height:1000,

    } ,
    wrap:{
        flexWrap: 'wrap',
        flexDirection:'row',
        paddingHorizontal: 15,
    },
    description: {
        fontSize: 14,
        paddingVertical: 15,
        lineHeight: 24,
        paddingHorizontal: 15,
    },
    text2:{
        fontSize:12,
        color:'white',
        fontFamily:'arial',
        position:'absolute',
        backgroundColor:`rgba(0, 0, 0, 0.3)`,
        padding:4,
        borderRadius:7,
        // bottom:600,
        alignSelf:'center',
        // top:Height*0.158,
        left:15
    },
    box:{
        // position: 'absolute',
        bottom:34,
        borderRadius:5,
    },
    button:{
        borderWidth: 1,
        borderColor: colors.border,
        borderRadius:10,
        backgroundColor:colors.white,
        elevation:10,
        marginHorizontal:15,
        marginVertical: 15,
    },
    title:{
        marginTop: 10,
        paddingHorizontal:15,
        fontSize:18,
        color:colors.primary,
        fontWeight: 'bold',
    },
    horizontal:{
        paddingVertical:5,
        paddingHorizontal: 10,
        // marginRight:10,
    },
    footer:{
        borderTopWidth:1,
        borderTopColor:colors.border,
        marginTop:10,
        paddingHorizontal:15,
        paddingVertical:15,
    },
    footerRow:{
        flexDirection:'row',
        justifyContent:'space-between',
        paddingBottom: 20,
    },
    footerText:{
        color:colors.second,
    },
    footerB: {
        // flex:0.5,
        borderWidth: 1,
        flexDirection: 'row',
        // justifyContent: 'center',
        alignItems: 'center',
        borderRadius:5,
        paddingHorizontal:10,
        borderColor:colors.border,
        // width:'50%'
        // borderWidth:1

    },
    buttonT:{
        marginHorizontal:10,
        paddingVertical:10,
        color:colors.second,
        // borderWidth:1,
    },
    footerWrap:{
        ...horizontal
    },
    items:{
        paddingHorizontal:4,
    }


})
export default styles
