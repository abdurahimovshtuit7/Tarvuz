import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from "react-native";
import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components';
// import Plus from '../../assets/SVG/TabIcon/Plus'
import NavigationService from "navigators/NavigationService";

//----------------SVG------------


class ListingOwner extends Component {
    state = {
    }

    render() {
        const {navigation} = this.props
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <ScrollView style={styles.component}>
                <View style={styles.button} >
                    <Components.Owner name={'Ivan Petrov'} listings={'Registered: June 2, 2020'} status={'Last seen - June 23, 14:44'} touch={()=>{
                        // NavigationService.navigate('owner',{title:"Listing Owner"});
                    }}/>
                </View>
                </ScrollView>
                <ScrollView style={styles.scroll}>
                    <View style={styles.view}>
                        <Components.Item/>
                        <Components.Item/>
                        <Components.Item/>
                        <Components.Item/>
                        <Components.Item/>
                        <Components.Item/>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

export default ListingOwner
