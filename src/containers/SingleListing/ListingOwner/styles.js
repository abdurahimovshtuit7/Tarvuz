import {StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor:colors.white,
    },

    view:{
        // flex:1,
        flexDirection: 'row',
        justifyContent:'space-between',
        // paddingLeft:10,
        marginHorizontal: 9,
        // borderWidth:1,
        // flexGrow:1
        flexWrap:'wrap'
    },
    component:{
        // flex:1,
        paddingVertical:12,
    },
    button:{
        flex:1,
        borderTopWidth: 1,
        borderTopColor: colors.border,
        // borderRadius:10,
        backgroundColor:colors.white,
        // elevation:10,
        // marginHorizontal:15,
        paddingBottom: 15,
    },
})

export default styles
