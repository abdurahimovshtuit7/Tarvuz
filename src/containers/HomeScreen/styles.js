import {StyleSheet,Dimensions} from 'react-native';
import colors from '../../assets/styles/colors'
const windowWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // marginVertical:10,
        backgroundColor: colors.white,
        // paddingBottom: 20
    },
    scroll: {
        // marginLeft: 0,
        // borderWidth:1,
    },
    search: {
        marginHorizontal: 15,
        marginVertical: 10,
        paddingHorizontal: 10,
        paddingVertical: 15,
        flexDirection: 'row',
        alignItems: 'center',
        // justifyContent:'space-around',
        backgroundColor: colors.background,
        borderRadius: 10
    },
    searchStyle: {
        fontSize: 14,
        paddingLeft: 10,
        color: colors.second,
    },
    content: {
        marginVertical: 10,
        paddingBottom: 20,
        // width:windowWidth,
        // alignContent:'stretch',
        // backgroundColor:'green',
        // flexDirection: 'row',
        // flexWrap: 'wrap',
        // alignItems: 'center',
        justifyContent: 'space-around',
        // marginHorizontal: 5,

        // flex:1,
        flexGrow: 1,
    },
    view: {
        flex:1,
        flexGrow:1,
        // flexDirection: 'column',
        // backgroundColor:'green',
        justifyContent: 'space-between',
        paddingHorizontal: 8,
        flexWrap: 'wrap',
        direction:'ltr'
    }

})

export default styles
