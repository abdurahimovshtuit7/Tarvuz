import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    ScrollView,
    Dimensions
} from "react-native";
import styles from './styles'
import colors from "../../assets/styles/colors";
import Components from 'components'
import {Routines} from "services/api";
import {connect} from "react-redux";
import {get} from 'lodash'
import {withTranslation} from "react-i18next";
import Translate from 'services/translateTitle'


import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";

//----------------SVG---------------------
import All from '../../assets/SVG/Categories/All'
import House from '../../assets/SVG/Categories/House'
import Car from '../../assets/SVG/Categories/Car'
import Lamp from '../../assets/SVG/Categories/Lamp'
import Animal from '../../assets/SVG/Categories/Animal'
import Bike from '../../assets/SVG/Categories/Bike'
import Work from '../../assets/SVG/Categories/Work'
import Search from '../../assets/SVG/Search'
import NavigationService from "../../navigators/NavigationService";

const windowWidth = Dimensions.get('window').width;
class Home extends Component {
    state = {
        icon: [
            {title: "All categories", item: <All size={90}/>, icon: <All size={40}/>, color: colors.white},
            {title: "The property", item: <House size={90}/>, icon: <House size={40}/>, color: colors.property},
            {title: "Transport", item: <Car size={90}/>, icon: <Car size={40}/>, color: colors.transport},
            {title: "Furniture", item: <Lamp size={90}/>, icon: <Lamp size={40}/>, color: colors.lamp},
            {title: "Animal", item: <Animal size={90}/>, icon: <Animal size={40}/>, color: colors.animal},
            {title: "Sport", item: <Bike size={90}/>, icon: <Bike size={40}/>, color: colors.sport},
            {title: "Work", item: <Work size={90}/>, icon: <Work size={40}/>, color: colors.work},

        ],
        isFetched: true,
        refreshing: false,
        page: 1,
        isActive:false,
        numColumns:2,
    }

    componentDidMount() {
        this.getItems(1)
        this.getCategories()
        this.favoritesList()
    }

    getCategories() {
        Routines.ads.getCategories({
            request: {}
        }, this.props.dispatch)
            .then((data) => {
                    // console.log("e", payload) bu funksiya api 200 qaytsa ishlidi va payload qaytadi agar siz redux ga saqlashni xoxlamasangiz shu component state ga yozib ishlatshingiz mumkin
                },
            )
    }
    favoritesList() {
        Routines.ads.favoritesList({
            request: {}
        }, this.props.dispatch)
            .then((data) => {
                    // console.log("e", payload) bu funksiya api 200 qaytsa ishlidi va payload qaytadi agar siz redux ga saqlashni xoxlamasangiz shu component state ga yozib ishlatshingiz mumkin
                },
            )
    }
    addFavorite(id,item) {
        Routines.ads.addFavorite({
            request: {
                data:{
                  id,
                  item
                }
            }
        }, this.props.dispatch)
            .then((data) => {

                },
            )
    }

    getItems(page = 1, isFetched = false, refreshing = true) {

        this.setState({isFetched, refreshing})
        Routines.ads.getItems({
            request: {
                page: page
            }
        }, this.props.dispatch)
            .then((data) => {
                    this.setState({isFetched: true, refreshing: false, page: page + 1})
                },
            )
            .catch(() => {
                this.setState({isFetched: true, refreshing: false})
            })
    }

    renderItems = () => {
        return get(this.props.items, 'data', [])
    }

    render() {
        const {navigation, items, categories,t,currentLangCode,favoritesList,favorites} = this.props
        const {refreshing, isFetched, page,numColumns} = this.state
        console.log("FavoritesList:", favoritesList)
        console.log("LANG:", currentLangCode)
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <TouchableOpacity style={styles.search}>
                    <Search/>
                    <Text style={styles.searchStyle}>
                        {t('Search')}
                    </Text>
                </TouchableOpacity>
                <Components.InfinityFlatList
                    contentContainerStyle={styles.content}
                    numColumns={2}
                    // horizontal ={false}
                    columnWrapperStyle={styles.view}
                    data={this.renderItems()}
                    renderItem={
                        ({item, index}) => {
                                return (
                                    <Components.Item key={item.id}
                                                     index={index}
                                                     width ={(index+1)%7===0?'100%':windowWidth/2.17}
                                                     item={item}
                                                     saved={(id,item)=>{
                                                         this.addFavorite(id,item)
                                                     }}
                                                     navigate={()=>{
                                                         NavigationService.navigate('singleListing', {
                                                         item: item
                                                     })}}
                                                     list = {get(this.props,'favoritesList',{})}

                                    />
                                )
                        }
                    }
                    loading={!isFetched}
                    refreshing={refreshing}
                    onRefresh={() => {
                        this.getItems(1, true, false)
                    }}
                    onEndReached={() => {
                        if (get(this.props.items, 'links.last_page', 1) >= page) {
                            console.log("PAGE:", page)
                            this.getItems(page, false, true)
                        }
                    }}
                    ListHeaderComponent={({item, index}) => {
                        return (
                            <View key={index}>
                                <ScrollView
                                    horizontal={true}
                                    style={styles.scroll}
                                    showsHorizontalScrollIndicator={false}
                                >
                                    <Components.Circle
                                        title={<All size={90}/>}
                                        text={t("All categories")}
                                        index={'0'}
                                        touch={() => {
                                            navigation.navigate('categories', {
                                                categories: categories
                                            })
                                        }
                                        }
                                    />
                                    {get(categories, 'data', []).map((item, index) => {
                                        return (<Components.Circle
                                            key={index}
                                            item={item}
                                            lang={currentLangCode}
                                            image={item.icon_image}
                                            text={Translate.title(currentLangCode,item.name_en,item.name_ru,item.name_uz)}
                                            index={index}
                                            touch={() => {
                                                navigation.navigate('category', {
                                                    // title:item.icon,
                                                    // text:item.title,
                                                    child: item,
                                                    index: index,
                                                    // name:item.name,
                                                    color: item.color
                                                })
                                            }
                                            }

                                        />)
                                    })}
                                </ScrollView>
                                {/*<Components.UnderlineTab/>*/}
                            </View>
                        )
                    }}
                    emptyText="No Items"
                    keyExtractor={(item, index) =>
                        index
                    }
                    onEndThreshold={0}
                />


            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        items: state.ads.items,
        categories: state.ads.categories,
        currentLangCode:state.language.lang,
        favoritesList:state.ads.favoritesList,
        favorites:state.ads.Favorite
    };
};

Home = connect(mapStateToProps)(Home)
export default withTranslation('main')(Home)
