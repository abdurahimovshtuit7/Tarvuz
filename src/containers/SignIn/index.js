import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from "react-native";
import styles from './styles'
import colors from "../../assets/styles/colors";
import MegaPolis from '../../assets/SVG/MegaPolis'
import Apple from '../../assets/SVG/Apple'
import Facebook from '../../assets/SVG/Facebook'
import Components from '../../components'
import NavigationService from "navigators/NavigationService";

class SignIn extends Component {
    render() {
        const {navigation} =this.props
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <ScrollView contentContainerStyle={styles.Scroll}>

                    <View style={styles.row}>
                        <Text style={styles.text}>Tarvuz</Text>
                        <TouchableOpacity onPress={()=>{
                            NavigationService.reset('tab',0)
                        }}>
                            <Text style={styles.textStep}>Skip this step</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.logo}>
                        <View style={styles.box}>
                            <MegaPolis style={styles.border}/>
                            {/*<Image source={require('../../assets/Artboard_9_1_1-removebg1.png')} style={{}}/>*/}
                        </View>
                    </View>

                    <Text style={styles.welcome}>
                        Welcome to Tarvuz
                    </Text>
                    <Text style={styles.atribut}>
                        CHOOSE HOW DO YOU WANT TO CONTINUE
                    </Text>

                    <Components.RegButton logo={<Apple/>} text={'Log in with Apple'} color={colors.white}/>
                    <Components.RegButton logo ={<Facebook/>} text={'Log in with Facebook'} color={colors.white}/>
                    <Components.SignButton text={'Sign up'} touch={()=>{navigation.navigate('signUp')}}/>

                    <TouchableOpacity onPress={()=>{navigation.navigate('login')}}>
                        <Text style={styles.bottom}>
                            I already have account
                        </Text>
                    </TouchableOpacity>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

export default SignIn
