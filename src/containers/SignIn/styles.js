import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        flex: 1,
        //alignItems: 'center',
        //justifyContent: 'space-between',
        backgroundColor:colors.white,

    },
    Scroll:{
       // paddingBottom:100,
        backgroundColor:colors.white
    },

    row:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop:'5%',
        //borderWidth:1,
        paddingHorizontal:'4%',
        alignItems:'center',
    },
    text:{
        color:colors.brandColor,
        fontWeight:'bold',
        fontSize:30
    },
    textStep:{
        color:colors.border,
        fontSize: 16,
        fontWeight: '600',
        paddingVertical:10
    },
    logo:{
        //borderWidth:1,
        alignItems: 'center',
        //marginTop:'10%',
        justifyContent: 'center',
       // paddingTop: '10%'
    },
    box:{
        width:'60%',
        height:230,
        backgroundColor: colors.gray,
        borderRadius:60,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: '8%',
        // paddingTop: '10%'
    },
    border:{
        // position:"relative",
        // borderWidth: 1,
        // elevation:10,
        overflow:"hidden"
    },
    welcome:{
        textAlign:'center',
        fontSize:30,
        fontWeight:'bold',
        color:colors.primary
    },
    atribut:{
        fontSize:12,
        fontWeight:'600',
        color:colors.textGray,
        textAlign:'center',
        marginTop: 10,
        letterSpacing: 0.1,
        marginBottom:"12%"
    },
    bottom:{
        textAlign:'center',
        color:colors.brandColor,
        fontSize:16,
        fontWeight:'600',
        paddingTop:"8%",
        marginBottom: 20
    }

})

export default styles
