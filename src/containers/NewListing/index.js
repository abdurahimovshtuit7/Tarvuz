import React, {Component} from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    SafeAreaView,
    ScrollView
} from "react-native";
import styles from './styles'
import Components from 'components'
import colors from "../../assets/styles/colors";
import {setTab} from 'services/actions';
import {connect} from 'react-redux';

import All from "../../assets/SVG/Categories/All";
import House from "../../assets/SVG/Categories/House";
import Car from "../../assets/SVG/Categories/Car";
import Lamp from "../../assets/SVG/Categories/Lamp";
import Animal from "../../assets/SVG/Categories/Animal";
import Bike from "../../assets/SVG/Categories/Bike";
import Work from "../../assets/SVG/Categories/Work";
import RotatePlus from 'assets/SVG/TabIcon/rotatePlus'
import NavigationService from "../../navigators/NavigationService";
import {Routines} from "../../services/api";
import {get} from "lodash";
import {withTranslation} from "react-i18next";
import Translate from "../../services/translateTitle";



class Index extends Component {
    state = {

    }
    componentDidMount() {
        this.getCategories()
    }
    getCategories(){
        Routines.ads.getCategories({
            request: {}
        }, this.props.dispatch)
            .then((data) => {
                    console.log("DATA:",data)
                    // console.log("e", payload) bu funksiya api 200 qaytsa ishlidi va payload qaytadi agar siz redux ga saqlashni xoxlamasangiz shu component state ga yozib ishlatshingiz mumkin
                },
            )
    }
    render() {
        const {navigation,categories,t,currentLangCode} = this.props

        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.component}>
                    <Text style={styles.text}>{t('Choose category')}</Text>
                    <Text style={styles.savedText}>{t('New listing')}</Text>
                </View>
                <ScrollView  style={styles.container}>
                    <View style={styles.view}>
                        {get(categories,'data',[]).map((item, index) => {
                            return (<Components.Card
                                color={item.color}
                                key={index}
                                image={item.icon_image}
                                text={Translate.title(currentLangCode,item.name_en,item.name_ru,item.name_uz)}
                                index={index}
                                touch={() => {
                                    (navigation.navigate('addCategory',{
                                        index:index,
                                        child:item,
                                        color:item.color,
                                        category:item.name_en
                                    }))
                                }
                                }

                            />)
                        })}
                    </View>
                </ScrollView>
                <View style={styles.footer}>
                    <Components.AddButton  background={colors.white} icon={<RotatePlus color={colors.brandColor}/>} border={colors.brandColor} touch={()=>{
                        navigation.goBack()
                    }}/>
                </View>
            </SafeAreaView>
        )
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        tabButton: state.tabButton.data,
        categories:state.ads.categories,
        currentLangCode:state.language.lang
    };
};

Index = connect(mapStateToProps)(Index)
export default withTranslation('main')(Index)
