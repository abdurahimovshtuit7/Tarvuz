import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'


const styles = StyleSheet.create({
    container: {
        flex: 1,
        // marginVertical:10,
        backgroundColor:colors.white,
        // borderWidth:1,
        // justifyContent:'center',
        paddingBottom: 70
    },
    component:{
        paddingHorizontal:15,
        paddingVertical:10,
    },
    view:{
        flex:1,
        // flex: 0.48,
        flexDirection: 'row',
        justifyContent:'space-between',
        marginHorizontal: 9,
        flexWrap:'wrap'
    },
    text:{
        fontSize: 12,
        color:colors.textGray,
        fontWeight:'600',
        paddingTop:6,
        textTransform: 'uppercase'
    },
    savedText:{
        fontSize:30,
        fontWeight: 'bold'
    },
    footer:{
        position:'absolute',
        // borderWidth:1,
        width:'100%',
        // height:60,
        flex:1,
        backgroundColor: colors.white,
        bottom:0,
        // justifyContent:'center',
        alignItems:'center',
        borderTopWidth:0.3,
        borderTopColor:colors.border,
        paddingBottom: 17,
        // borderWidth:1,
        height: 65,
        paddingTop: 10
    }
})
export default styles
