import {StyleSheet, Platform} from 'react-native';
import colors from '../../../assets/styles/colors'


const styles = StyleSheet.create({
    container: {
        flex: 1,
        // marginVertical:10,
        backgroundColor:colors.white,
        // borderWidth:1,
        // justifyContent:'center',
    },

    view:{
        flex:1,
        // flex: 0.48,
        flexDirection: 'row',
        justifyContent:'space-between',
        marginHorizontal: 9,
        flexWrap:'wrap'
    },
    row:{
        flexDirection:'row',
        // flex:1
        justifyContent: 'space-between',
        paddingHorizontal:18,
        paddingTop:16,
        paddingBottom:10
    },
    text:{
        fontSize:30,
        fontWeight:'bold',
        color:colors.primary,
        textAlign:'center'
    },
    textCancel:{
        fontSize:14,
        // fontWeight:'bold',
        color:colors.second,
        marginRight:20,
    }


})
export default styles
