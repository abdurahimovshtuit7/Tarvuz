import React, {Component} from 'react';
import {Text, View, ScrollView, TouchableOpacity} from 'react-native';
import {Icon} from 'native-base/src/basic/Icon/index';
import Ionicons from 'react-native-vector-icons/MaterialIcons'
import AnimatedHeader from 'react-native-animated-header';
import colors from "../../../assets/styles/colors";
import Components from "../../../components";
import styles from "./styles";
import {SafeAreaView} from "react-native-safe-area-context";

import Animal from "../../../assets/SVG/Categories/Animal";

import Work from "../../../assets/SVG/Categories/Work";
// import Bg from './assets/bg.jpg';


export default class App extends Component {
    state = {
        title: [
            {title: "All on The property", item: "84 235 listings"},
            {title: "Apartments", item: "43 843 listings"},
            {title: "House", item: "2 439 listings"},
            {title: "Room", item: "8 854 listings"},
            {title: "Land", item: "5 184 listings"}
        ]
    }

    render() {
        const {navigation, route} = this.props

        return (
            <SafeAreaView style={styles.container}>
                <AnimatedHeader
                    style={{flex: 1}}
                    // backText='Back'
                    title='Category'
                    parallax={true}
                    renderLeft={() => (<Ionicons name="arrow-back" size={25} style={{marginLeft: 20}} onPress={() => {
                        navigation.goBack()
                    }}/>)}

                    renderRight={() => (
                        <TouchableOpacity>
                            <Text style={styles.textCancel}>
                                Cancel
                            </Text>
                        </TouchableOpacity>
                    )
                    }
                    backStyle={{marginLeft: 10}}
                    // backTextStyle={{fontSize: 14, color: '#000'}}
                    titleStyle={{fontSize: 30, left: 20, bottom: 20, color: colors.primary, fontWeight: 'bold'}}
                    headerMaxHeight={120}
                    // imageSource={()=>{
                    //
                    // }}
                    toolbarColor='#FFF'
                    disabled={false}
                    noBorder={true}
                >
                    {/*<Left>*/}
                    {/*    <Icon name='md-arrow-back' style={{marginLeft: 20}} onPress={() => {*/}
                    {/*        navigation.goBack()*/}
                    {/*    }}/>*/}
                    {/*</Left>*/}
                    <ScrollView style={styles.container}>
                        <Components.CategoryCard
                            title={route.params.title}
                            text={route.params.text}
                            index={route.params.index}
                            name={route.params.name}
                            color={route.params.color}
                        />
                        {
                            this.state.title.map((item, index) => {
                                return (<Components.CardList
                                    key={index}
                                    title={item.title}
                                    item={item.item}

                                />)
                            })
                        }


                    </ScrollView>
                </AnimatedHeader>
            </SafeAreaView>
        );
    }
}
