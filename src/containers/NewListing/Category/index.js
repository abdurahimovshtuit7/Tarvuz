import React, {Component} from 'react';
import {Text, View, ScrollView, TouchableOpacity} from 'react-native';
import {Icon} from 'native-base/src/basic/Icon/index';
import Ionicons from 'react-native-vector-icons/MaterialIcons'
import AnimatedHeader from 'react-native-animated-header';
import colors from "../../../assets/styles/colors";
import Components from "../../../components";
import styles from "./styles";
import {SafeAreaView} from "react-native-safe-area-context";

//----------------SVG--------------------
import RotatePlus from "../../../assets/SVG/TabIcon/rotatePlus";
import RightArrow from 'assets/SVG/TabIcon/RightArrow'
import NavigationService from "../../../navigators/NavigationService";
import {get} from "lodash";
import Translate from "../../../services/translateTitle";
import {connect} from "react-redux";
import {withTranslation} from "react-i18next";



 class AddCategory extends Component {
    state = {
        title: [
            {title: "All on The property", item: "84 235 listings"},
            {title: "Apartments", item: "43 843 listings"},
            {title: "House", item: "2 439 listings"},
            {title: "Room", item: "8 854 listings"},
            {title: "Land", item: "5 184 listings"},
        ]
    }

    render() {
        const {navigation, route,currentLangCode,t} = this.props
        const child = get(this.props.route.params,'child',{})
        return (
           <View style={styles.container}>
                <AnimatedHeader
                    style={{flex: 1}}
                    title={t('Category')}
                    parallax={true}
                    titleStyle={{fontSize: 30, left: 20, bottom: 20, color: colors.primary, fontWeight: 'bold'}}
                    headerMaxHeight={100}
                    toolbarColor='#FFF'
                    disabled={false}
                    noBorder={true}
                >
                    <ScrollView contentContainerStyle={styles.container}>
                        <Components.CategoryCard
                            image={child.icon_image}
                            text={child.name_en}
                            index={route.params.index}
                            name={''}
                            color={route.params.color}
                            title={route.params.title}
                        />
                        {
                            get(child,'children',[]).map((item, index) => {
                                return (<Components.CardList
                                    key={index}
                                    title={Translate.title(currentLangCode,item.name_en,item.name_ru,item.name_uz)}
                                    // item={'4 439 listings'}
                                    touch ={()=>{
                                        NavigationService.navigate('addNew',{
                                            // item:item.title,
                                            // title:route.params.title,
                                            // text:route.params.text,
                                            index:route.params.index,
                                            color:route.params.color,
                                            child:item,
                                            category:route.params.category
                                        })
                                    }}
                                />)
                            })
                        }

                    </ScrollView>

                </AnimatedHeader>
                <View style={styles.footer}>
                    <Components.AddButton  background={colors.white} icon={<RightArrow color={colors.brandColor}/>} border={colors.brandColor} touch={()=>{
                        navigation.goBack()
                    }}/>
                </View>
           </View>
        );
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        items:state.ads.items,
        // categories:state.ads.categories,
        currentLangCode:state.language.lang,
    };
};

AddCategory= connect(mapStateToProps)(AddCategory)
export default withTranslation('main')(AddCategory)
