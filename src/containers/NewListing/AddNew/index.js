import React, {Component} from 'react';
import {Text, View, ScrollView, TouchableOpacity} from 'react-native';
import {Icon} from 'native-base/src/basic/Icon/index';
import Ionicons from 'react-native-vector-icons/MaterialIcons'
import AnimatedHeader from 'react-native-animated-header';
import colors from "../../../assets/styles/colors";
import Components from "../../../components";
import styles from "./styles";
import {SafeAreaView} from "react-native-safe-area-context";

//----------------SVG--------------------
import RotatePlus from "../../../assets/SVG/TabIcon/rotatePlus";
import RightArrow from 'assets/SVG/TabIcon/RightArrow'
import {get} from "lodash";
import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import Translate from "../../../services/translateTitle";



class AddNew extends Component {
    state = {
        title: [
            {title: "Long term rental", item: "84 235 listings"},
            {title: "Daily rent", item: "43 843 listings"},
            {title: "Sell", item: "2 439 listings"},
            {title: "Exchange", item: "8 854 listings"},
        ]
    }

    render() {
        const {navigation, route,currentLangCode} = this.props
        const child = get(this.props.route.params,'child',{})

        return (
            <SafeAreaView style={styles.container}>
                <Text style={styles.title}>
                    Categories
                </Text>

                    <ScrollView style={styles.container}>
                        <Components.CategoryCard
                            image={child.icon_image}
                            text={Translate.title(currentLangCode,child.name_en,child.name_ru,child.name_uz)}
                            index={route.params.index}
                            name={route.params.category}
                            color={route.params.color}
                        />
                        {
                            get(route.params.child,'children',[]).map((item, index) => {
                                return (<Components.CardList
                                    key={index}
                                    title={item.name_en}
                                />)
                            })
                        }


                    </ScrollView>

                <View style={styles.footer}>
                    <Components.AddButton  background={colors.white} icon={<RightArrow color={colors.brandColor}/>} border={colors.brandColor} touch={()=>{
                        navigation.goBack()
                    }}/>
                </View>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        tabButton: state.tabButton.data,
        categories:state.ads.categories,
        currentLangCode:state.language.lang
    };
};

AddNew = connect(mapStateToProps)(AddNew)
export default withTranslation('main')(AddNew)
