import {StyleSheet, Platform} from 'react-native';
import colors from '../../../assets/styles/colors'


const styles = StyleSheet.create({
    container: {
        flex: 1,
        // marginVertical:10,
        backgroundColor: colors.white,
        // borderWidth:1,
        // justifyContent:'center',
        // paddingBottom:30
    },
    title: {
        fontSize: 30,
        left: 20,
        bottom: 20,
        color: colors.primary,
        fontWeight: 'bold'
    },
    footer: {
        position: 'absolute',
        width: '100%',
        flex: 1,
        bottom: 0,
        alignItems: 'center',
        borderTopWidth: 0.3,
        borderTopColor: colors.border,
        paddingBottom: 17,
        height: 65,
        paddingTop: 10,
        backgroundColor: colors.white
    }


})
export default styles
