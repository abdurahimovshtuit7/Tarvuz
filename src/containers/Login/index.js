import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from "react-native";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {Formik, Field, Form, withFormik} from 'formik';
import * as Yup from 'yup'


import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components'
import {Routines} from "services/api";
import SignUp from "../SignUp";
import {get} from 'lodash'
import {connect} from "react-redux";
import {saveUser} from "services/actions";
import NavigationService from "navigators/NavigationService";

class Login extends Component {

    render() {
        const {handleSubmit,userData} = this.props
        console.log("USER_DATA: ",userData)
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" enableOnAndroid={false}
                                         contentContainerStyle={styles.wrapper}>
                    <Components.Layout style={{flex: 1}}>
                        <Text style={styles.welcome}>
                            Welcome back!
                        </Text>
                        <Field
                            name={'first_name'}
                            label={'First name'}
                            placeHolder={'Username'}
                            component={Components.Input}
                        />
                        <Field
                            name={'password'}
                            label={'First name'}
                            placeHolder={'Password'}
                            component={Components.Input}
                        />

                        <Components.SignButton text={'Log in'} margin={"4%"} touch={() => {
                            handleSubmit()
                        }}/>
                        <TouchableOpacity style={styles.button}
                                          onPress={() => {
                                              navigation.navigate('login')
                                          }}>
                            <Text style={styles.bottom}>
                                Forgot password
                            </Text>
                        </TouchableOpacity>
                    </Components.Layout>

                </KeyboardAwareScrollView>
            </SafeAreaView>
        )
    }
}

Login = withFormik({
    mapPropsToValues: () => ({
        first_name: "",
        password: ""
    }),
    validationSchema: ({t}) => {
        return Yup.object().shape({
            first_name: Yup.string().required("required"),
            // .matches(/^[a-z]+^[A-Z]+$/ , 'Is not in correct format'),
            password: Yup.string()
                .min(6, 'Belgilar soni kam!')
                .max(16, 'Too Long!')
                .required('required'),
        });
    },
    handleSubmit: (values, {props, setSubmitting}) => {
        console.log(values);

        let {first_name, password} = values;

        // let onlyNums = phone.replace(/[^\d]/g, '');
        // console.log("PHONE: ", onlyNums)
        // setSubmitting(true);
        Routines.auth.Login({
            request: {
                data: {
                    username: first_name,
                    password: password
                }
            }
        }, props.dispatch)
            .then((data) => {
                console.log("Login: ",props.login),
                props.dispatch(saveUser(props.login))
                    if (get(props.userData,'data.access_token','')){
                        // console.log("TOKEN:",get(props.userData,'data.access_token',''),
                NavigationService.reset('tab', 0,{
                    // phone,
                    // countryCode: code
                })}
            })
            .catch(e => {
                // setSubmitting(false);
                if (e.message === "NETWORK_ERROR") {
                }
            });
    }
})(Login);
const mapStateToProps = (state, ownProps) => {
    return {
        userData:state.user.user,
        login:state.profile.Login
    };
};

export default connect(mapStateToProps)(Login)
