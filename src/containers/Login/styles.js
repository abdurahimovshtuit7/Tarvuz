import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        flex: 1,
        //alignItems: 'center',
        //justifyContent: 'space-between',
        backgroundColor:colors.white

    },

    wrapper: {
        // flex:1,
        flexGrow:1,
        paddingVertical: 16
    },

    welcome:{
        textAlign:'center',
        fontSize:30,
        fontWeight:'bold',
        color:colors.primary,
        marginBottom: '10%'
    },

    button:{
        marginTop:"50%",
        marginHorizontal:50
    },
    bottom:{
        textAlign:'center',
        color:colors.brandColor,
        fontSize:16,
        fontWeight:'600',

        //borderWidth:1,

    }

})

export default styles
