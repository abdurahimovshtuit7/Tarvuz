import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor:colors.white,
    },
    wrapper: {
        // flex:1,
        flexGrow:1,
        paddingVertical: 26
    },
    welcome:{
        textAlign:'center',
        fontSize:30,
        fontWeight:'bold',
        color:colors.primary,
        marginBottom: '10%'
    },
    bottom:{
        textAlign:'center',
        color:colors.second,
        fontSize:12,
        fontWeight:'600',
        marginHorizontal:10,
    },
    inline:{
        fontWeight:'bold'
    }

})

export default styles
