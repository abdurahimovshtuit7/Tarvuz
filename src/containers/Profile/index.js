import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from "react-native";
import styles from './styles'
import colors from "../../assets/styles/colors";
import Components from '../../components'
import Edit from '../../assets/SVG/Edit'
import moment from "moment";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";

//----------------SVG---------------------
import All from '../../assets/SVG/Categories/All'
import House from '../../assets/SVG/Categories/House'
import Car from '../../assets/SVG/Categories/Car'
import Lamp from '../../assets/SVG/Categories/Lamp'
import Animal from '../../assets/SVG/Categories/Animal'
import Bike from '../../assets/SVG/Categories/Bike'
import Work from '../../assets/SVG/Categories/Work'
import Search from '../../assets/SVG/Search'
import NavigationService from "../../navigators/NavigationService";
import {connect} from "react-redux";
import {withTranslation} from "react-i18next";

class Profile extends Component {
    state = {
        icon: [
            {title: "All categories", item: <All size={90}/>},
            {title: "The property", item: <House size={90}/>},
            {title: "Transport", item: <Car size={90}/>},
            {title: "Furniture", item: <Lamp size={90}/>},
            {title: "Animal", item: <Animal size={90}/>},
            {title: "Sport", item: <Bike size={90}/>},
            {title: "Work", item: <Work size={90}/>},
        ]
    }

    render() {
        const {navigation,t} = this.props
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <View style={styles.component}>
                    <Text style={styles.text}>{moment().format('dddd D MMM')}</Text>
                    <Text style={styles.savedText}>{t('Profile')}</Text>
                </View>
                <ScrollView>
                    <View style={styles.row}>
                        <Components.UserImage style={styles.image}
                            uri={ require('../../assets/images/Ellipse.png')}
                                              username={"Alex Murphy"}
                        >
                            <View style={styles.icon}>
                                <Edit/>
                            </View>
                        </Components.UserImage>
                        <View style={styles.username}>
                            <Text style={styles.profile} numberOfLines={1}>
                                Alex Murphy
                            </Text>
                            <Text style={styles.date}  numberOfLines={1}>
                                Registered: June 2, 2020
                            </Text>
                        </View>
                    </View>

                    <Components.Balance
                        // title={route.params.title}
                         text={"$ 988"}
                         balance={t('Balance')}
                        // index={route.params.index}
                        name={t('Add Funds')}
                        color={colors.backgroundColor}
                         touch={()=>{
                             NavigationService.navigate('addfunds',{title:t('Add Funds')})
                         }}
                    />

                    <View style={styles.view}>
                       <Components.ProfileButton title={t('My Active Listings')}
                                                 active={colors.brandColor}
                                                 item={15}
                                                 touch={()=>{
                                                     NavigationService.navigate('active',{title:t('My Active Listings')})
                                                 }}
                       />
                        <View style={styles.border}/>
                       <Components.ProfileButton title={t('My Archived Listings')}
                                                 active={colors.second}
                                                 item={4}
                                                 touch={()=>{
                                                     NavigationService.navigate('archive',{title:t('My Archived Listings')})
                                                 }}
                       />
                    </View>

                    <View style={styles.view}>
                        <Components.ProfileButton title={t('Account Details')}/>
                        <View style={styles.border}/>
                        <Components.ProfileButton title={t('Settings')}
                                                  touch={()=>{
                                                      NavigationService.navigate('settings',{title:t('Settings')})
                                                  }}
                        />
                        <View style={styles.border}/>
                        <Components.ProfileButton title={t('Payments')}
                                                  touch={()=>{
                                                      NavigationService.navigate('payment',{title:t('Payments')})
                                                  }}
                        />
                        <View style={styles.border}/>
                        <Components.ProfileButton title={t('Contact')}/>
                        <View style={styles.border}/>
                        <Components.ProfileButton title={t('Terms of use')}/>

                    </View>
                    <Text style={styles.footer}>Tarvuz, inc 2020</Text>
                    <Text style={styles.version}>Version 0.4.2</Text>
                    <TouchableOpacity style={styles.button}>
                        <Text style={styles.logout}>
                            {t('Log Out')}
                        </Text>
                    </TouchableOpacity>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        items: state.ads.items,
        favorites:state.ads.Favorite
    };
};
Profile = connect(mapStateToProps)(Profile)
export default withTranslation('main')(Profile)
