import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from "react-native";
import styles from './styles'
import colors from "../../assets/styles/colors";
import Components from '../../components'
import Plus from '../../assets/SVG/TabIcon/Plus'

//----------------SVG------------

const PROP = [
    {
        key: '1',
        text: '$ 3',
    },
    {
        key: '2',
        text: '$ 5',
    },
    {
        key: '3',
        text: '$ 15',
    },
    {
        key: '4',
        text: '$ 25',
    },

];

class AddFunds extends Component {
    state = {
    }

    render() {
        const {navigation} = this.props
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <ScrollView style={styles.scroll}>
                   <Components.Funds PROP={PROP}/>
                    <Text style={styles.text}>
                        Payment Method
                    </Text>
                    <TouchableOpacity style={styles.row}>
                        <Plus color={colors.brandColor} height={10} width={10}/>
                        <Text style={styles.textButton}>Add New Card...</Text>
                    </TouchableOpacity>
                   <View style={styles.view}>
                    <Components.Add text={'AddFunds'} margin={'22%'}/>
                   </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

export default AddFunds
