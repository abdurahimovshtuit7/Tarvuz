import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor:colors.white,
    },
    scroll:{
        // marginLeft:0,
        // borderWidth:1,
        flex:1,
        // justifyContent: 'flex-end',
    },
    border:{
        borderWidth:0.5,
        flex:1,
        borderColor:colors.border,
    },
    text:{
        fontSize:18,
        color:colors.primary,
        paddingHorizontal:15,
        marginVertical:15,
        fontWeight:'700'
    },
    row:{
        flexDirection:'row',
        backgroundColor:colors.white,
        paddingHorizontal: 20,
        alignItems:'center',
        marginHorizontal:15,
        borderRadius:10,
        paddingVertical:25,
        // borderWidth: 1,
        elevation:7,
        marginBottom:10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,

    },
    textButton:{
        paddingLeft:10,
        color: colors.brandColor
    },
    view:{
        justifyContent:'flex-end'
    }

})

export default styles
