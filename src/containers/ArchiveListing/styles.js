import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor:colors.view,
    },
    scroll:{
        marginLeft:0,
        // borderWidth:1,
    },
    border:{
        borderWidth:0.5,
        flex:1,
        borderColor:colors.border,

    },

})

export default styles
