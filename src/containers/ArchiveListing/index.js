import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from "react-native";
import styles from './styles'
import colors from "../../assets/styles/colors";
import Components from '../../components'

import Reactivate from '../../assets/SVG/Profile/Reactivate'
import Delete from '../../assets/SVG/Profile/Delete'
import {connect} from "react-redux";
import {withTranslation} from "react-i18next";

class ArchiveListing extends Component {
    state = {
    }
    render() {
        const {navigation,t} = this.props
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <ScrollView>
                <Components.ArchiveButton title={'Apartment for rent, Du...'}
                                          item = {'Active: May 3 - May 22'}
                                          balance={'$ 2 449'}
                                          icon1={<Reactivate/>}
                                          icon2={<Delete/>}
                                          text1={t('Reactivate')}
                                          text2={t('Delete')}
                                          color={colors.red}
                />
                    <Components.ArchiveButton title={'2bd, Flatbush Ave'}
                                              item = {'Active: May 3 - May 22'}
                                              balance={'$ 1 293'}
                                              icon1={<Reactivate/>}
                                              icon2={<Delete/>}
                                              text1={t('Reactivate')}
                                              text2={t('Delete')}
                                              color={colors.red}
                    />

                </ScrollView>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        items: state.ads.items,
        favorites:state.ads.Favorite
    };
};
ArchiveListing = connect(mapStateToProps)(ArchiveListing)
export default withTranslation('main')(ArchiveListing)
