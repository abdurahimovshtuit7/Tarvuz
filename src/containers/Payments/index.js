import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from "react-native";
import styles from './styles'
import colors from "../../assets/styles/colors";
import Components from '../../components'

//----------------SVG------------


class Payments extends Component {
    state = {
    }

    render() {
        const {navigation} = this.props
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <ScrollView>
                   <Components.Payment  title={'- $ 29'}
                                        item={'Golden Package'}
                                        date={'06.09.2020'}
                                        number={'58649'}
                                        color={colors.red}
                   />
                   <Components.Payment title={'+ $ 200'}
                                       item={'Add funds'}
                                       date={'06.06.2020'}
                                       number={'58649'}
                                       color={colors.green}

                   />

                </ScrollView>
            </SafeAreaView>
        )
    }
}

export default Payments
