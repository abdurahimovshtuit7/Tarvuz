import React, {Component} from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    SafeAreaView,
    ScrollView
} from "react-native";
import styles from './styles'
import Components from 'components'
import {get} from 'lodash'
import All from "assets/SVG/Categories/All";
import House from "assets/SVG/Categories/House";
import Car from "assets/SVG/Categories/Car";
import Lamp from "assets/SVG/Categories/Lamp";
import Animal from "assets/SVG/Categories/Animal";
import Bike from "assets/SVG/Categories/Bike";
import Work from "assets/SVG/Categories/Work";
import colors from "assets/styles/colors";
import {connect} from "react-redux";
import {Routines} from "services/api";
import NavigationService from "navigators/NavigationService";


class Categories extends Component {
    state = {
        icon: [
            {title: "All categories", item: <All size={40}/>, color: colors.white},
            {title: "The property", item: <House size={40}/>, color: colors.property},
            {title: "Transport", item: <Car size={40}/>, color: colors.transport},
            {title: "Furniture", item: <Lamp size={40}/>, color: colors.lamp},
            {title: "Animal", item: <Animal size={40}/>, color: colors.animal},
            {title: "Sport", item: <Bike size={40}/>, color: colors.sport},
            {title: "Work", item: <Work size={40}/>, color: colors.work},
        ]
    }
    componentDidMount() {
        this.getCategories()
    }

    getCategories(){
        Routines.ads.getCategories({
            request: {}
        }, this.props.dispatch)
            .then((data) => {
                console.log("DATA:",data)
                    // console.log("e", payload) bu funksiya api 200 qaytsa ishlidi va payload qaytadi agar siz redux ga saqlashni xoxlamasangiz shu component state ga yozib ishlatshingiz mumkin
                },
            )
    }
    render() {
        const {navigation,categories} = this.props
        console.log("Categories:",categories)
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.row}>
                    <Text style={styles.text}>
                        All categories
                    </Text>
                    <TouchableOpacity  onPress={()=>{navigation.goBack()}}
                    >
                        <Text style={styles.textCancel}>
                            Cancel
                        </Text>
                    </TouchableOpacity>
                </View>
                <ScrollView>
                    <View style={styles.view}>
                        {get(categories,'data',[]).map((item, index) => {
                            return (<Components.Card
                                // name={item.name}
                                color={item.color}
                                key={index}
                                image={item.icon_image}
                                text={item.name_en}
                                index={index}
                                touch={() => {
                                    NavigationService.navigate('category',{
                                        index:index,
                                        child:item,
                                        color:item.color
                                    })
                                }
                                }

                            />)
                        })}
                    </View>
                </ScrollView>
            </SafeAreaView>

        )
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        items:state.ads.items,
        categories:state.ads.categories
    };
};

export default connect(mapStateToProps)(Categories)
