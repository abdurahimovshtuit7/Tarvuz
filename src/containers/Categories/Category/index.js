import React, {Component} from 'react';
import {Text, View, ScrollView, TouchableOpacity,SafeAreaView} from 'react-native';
import Ionicons from 'react-native-vector-icons/MaterialIcons'
import AnimatedHeader from 'react-native-animated-header';
import colors from "assets/styles/colors";
import Components from "components";
import {get} from 'lodash'
import styles from "./styles";

import NavigationService from "navigators/NavigationService";
import {connect} from "react-redux";
import Translate from 'services/translateTitle'
import {withTranslation} from "react-i18next";

class Category extends Component {
    state = {
    }

    render() {
        const {navigation, route,currentLangCode,t} = this.props
        const child = get(this.props.route.params,'child',{})
        console.log("Children:",get(route.params.child,'children',[]))
        return (
                <AnimatedHeader
                    style={{flex: 1, backgroundColor:colors.white,}}
                    title={t('Category')}
                    parallax={true}
                    renderLeft={() => (<Ionicons name="arrow-back" size={25} style={{marginLeft: 20}} onPress={() => {
                        navigation.goBack()
                    }}/>)}

                    renderRight={() => (
                        <TouchableOpacity onPress={()=>{
                            NavigationService.goBack()
                        }}>
                            <Text style={styles.textCancel}>
                                {t('Cancel')}
                            </Text>
                        </TouchableOpacity>
                            )
                        }
                    backStyle={{marginLeft: 10}}
                    titleStyle={{fontSize: 30, left: 25, bottom: 5, color: colors.primary, fontWeight: 'bold'}}
                    headerMaxHeight={120}
                    toolbarColor='#FFF'
                    disabled={false}
                    noBorder={true}
                >
                    <ScrollView contentContainerStyle={styles.container} >
                        <Components.CategoryCard
                            image={child.icon_image}
                            text={Translate.title(currentLangCode,child.name_en,child.name_ru,child.name_uz)}
                            index={route.params.index}
                            name={''}
                            color={route.params.color}
                        />
                        <Components.CardList
                            title={t('All on The property')}
                            item={'2 439 listings'}
                        />
                        {
                           get(child,'children',[]).map((item, index) => {
                                return (<Components.CardList
                                    key={index}
                                    title={Translate.title(currentLangCode,item.name_en,item.name_ru,item.name_uz)}
                                    item={'4 439 listings'}
                                />)
                            })
                        }
                    </ScrollView>
                </AnimatedHeader>
        );
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        items:state.ads.items,
        // categories:state.ads.categories,
        currentLangCode:state.language.lang,
    };
};

Category= connect(mapStateToProps)(Category)
export default withTranslation('main')(Category)
