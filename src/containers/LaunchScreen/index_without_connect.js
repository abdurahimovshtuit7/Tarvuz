import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {ActivityIndicator, Image, SafeAreaView, StatusBar, View} from "react-native";
import colors from "../../assets/styles/colors";
import Logo from '../../assets/SVG/Logo'

class LaunchScreen extends Component {

   render () {
    return (
        <SafeAreaView style={{
            flex:1,
            alignItems: 'center',
            justifyContent: 'center',
            //marginTop: 170,
            //paddingBottom: 30,
            backgroundColor:colors.brandColor
        }}>
            <StatusBar backgroundColor={colors.brandColor} barStyle="light-content"/>
            <Logo/>
        </SafeAreaView>
    )
  }
}

export default LaunchScreen
