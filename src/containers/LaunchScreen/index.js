import React, { Component } from 'react'
//import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import {
    View,
    ActivityIndicator,
    Platform
} from 'react-native'
//import { StackActions } from '@react-navigation/native';
//import { NavigationActions } from '@react-navigation/compat';
//import {Routines} from "../../services/api";
import colors from "../../assets/styles/colors";
import NavigationService from "../../navigators/NavigationService";

class LaunchScreen extends Component {
    componentDidMount () {
        // const resetAction = StackActions.reset({
        //     index: 0,
        //     actions: [NavigationActions.navigate({ routeName: 'initialScreen' })],
        // });
        // this.props.navigation.dispatch(resetAction);

        NavigationService.reset('initialScreen',0)
    }
    render () {
        // console.warn(JSON.stringify(this.props.login))
        return (
            <View style={{
                flex:1,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor:colors.brandColor
            }}>
                <ActivityIndicator size={Platform.OS === 'ios'?1:30} color={colors.gray}/>
            </View>
        )
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        //login: state.profile.data
    }
}
export default connect(mapStateToProps)(LaunchScreen)
