import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity
} from "react-native";
import styles from './styles'
import colors from "../../assets/styles/colors";
import NavigationService from "../../navigators/NavigationService";
import MegaPolis from 'assets/SVG/MegaPolis'

//import Logo from '../../assets/SVG/Logo'

class InitialScreen extends Component {
    render() {
        const { navigation } = this.props;
        console.log("Navigation:",navigation)
        return (
            <SafeAreaView style={styles.container}>
                <Text style={styles.text}>
                    Tarvuz
                </Text>
                {/*<MegaPolis style={styles.border}/>*/}

                <Text style={styles.textLang}>
                     CHOOSE LANGUAGE
                 </Text>

                <View style={styles.rows}>
                    <TouchableOpacity style={styles.touchable}
                                       onPress={()=>{navigation.navigate('signIn')}}
                    >
                        <Text style={styles.lang}>ENG</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.touchable,{
                        borderLeftColor:'rgba(255,255, 255, .1)',
                        borderLeftWidth:1,}]}
                                      onPress={()=>{navigation.navigate('signIn')}}
                    >
                        <Text style={styles.lang}>O'ZB</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.touchable,{
                        borderLeftColor:'rgba(255,255, 255, .1)',
                        borderLeftWidth:1,}]}
                                      onPress={()=>{navigation.navigate('signIn')}}
                    >
                        <Text style={styles.lang}>RUS</Text>
                    </TouchableOpacity>
                </View>


            </SafeAreaView>
        )
    }
}

export default InitialScreen
