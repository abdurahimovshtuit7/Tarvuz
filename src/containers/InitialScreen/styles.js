import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        flex: 1,
        //alignItems: 'center',
        //justifyContent: 'space-between',
          backgroundColor:colors.brandColor

    },
    text:{
        color:colors.white,
        marginTop:'30%',
        textAlign:'center',
        fontSize:30,
        fontWeight:'bold'
    },
    textLang:{
        textAlign: 'center',
        marginTop:341,
        fontSize: 12,
        color:colors.white,
        opacity: 0.3,
    },

    rows:{
        flexDirection:'row',
       // paddingHorizontal:80,
       //  alignItems:'center',
        marginTop:72,
        justifyContent:'space-around',
       // borderWidth:1,
        //paddingVertical:10,
        paddingHorizontal:'21%'
    },
    touchable:{
        //width:'30%'
       // borderWidth: 1,
        paddingVertical: 10,
    },
    lang:{
        color:colors.white,
        fontWeight: 'bold',
        paddingHorizontal: 23
    }
})

export default styles
