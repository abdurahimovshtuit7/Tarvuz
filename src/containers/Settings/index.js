import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from "react-native";
import RadioButtonRN from 'radio-buttons-react-native';


import styles from './styles'
import colors from "../../assets/styles/colors";
import Components from '../../components'
import {SET_LANGUAGE} from "../../services/constants";
import {connect} from "react-redux";
import {withTranslation} from "react-i18next";

class Settings extends Component {
    state = {

    }
    onLangClicked = (lang) => {
        console.log("LANG:",lang)
        let {i18n,dispatch} = this.props;
        i18n.changeLanguage(lang,()=>{
            dispatch({type:SET_LANGUAGE,lang})
        })
    }

    render() {
        const {navigation,currentLangCode} = this.props
        console.log("lang:",currentLangCode)
        const PROP = [
            {
                key: 'en',
                text: 'English',
            },
            {
                key: 'ru',
                text: 'Русский',
            },
            {
                key: 'uz',
                text: 'O’zbekcha',
            },

        ];
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <View style={styles.view}>
                <ScrollView>
                    <View style={styles.border}/>
                  <Components.Switch title={'Allow Push Notifications'} item={'By turning on notifications you will receive notifications of new chat messages'}/>
                    <View style={styles.border}/>
                    <Components.Switch title={'Use system preferences'} item={'a dark theme will turn on automatically depending on the settings of the smartphone'}/>
                    <View style={styles.border}/>
                    <Text style={styles.text}>
                        Language
                    </Text>
                    <Text style={styles.text1}>
                        Set as default language
                    </Text>
                     <Components.RadioButton PROP={PROP} touch={(value)=>{
                         this.onLangClicked(value)
                     }}
                     lang={currentLangCode}
                     />
                </ScrollView>
            </View>
            </SafeAreaView>
        )
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        currentLangCode:state.language.lang
    };
};

Settings = connect(mapStateToProps)(Settings)
export default withTranslation('main')(Settings)
