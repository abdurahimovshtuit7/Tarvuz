import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor:colors.white,
        // paddingHorizontal:15
    },
    view:{
        flex: 1,
        backgroundColor:colors.white,
        paddingHorizontal:15
    },
    scroll:{
        marginLeft:0,
        // borderWidth:1,
    },
    border:{
        borderWidth:0.5,
        flex:1,
        borderColor:colors.border,
    },
    text:{
        paddingTop:25,
        fontSize:16,
        fontWeight:'700',
    },
    text1:{
        fontSize: 14,
        color:colors.second,
        paddingTop: 20
    }

})

export default styles
