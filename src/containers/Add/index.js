import React, {Component} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from "react-native";
import styles from './styles'
import colors from "../../assets/styles/colors";
import Components from '../../components'
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";

//----------------SVG---------------------
import All from '../../assets/SVG/Categories/All'
import House from '../../assets/SVG/Categories/House'
import Car from '../../assets/SVG/Categories/Car'
import Lamp from '../../assets/SVG/Categories/Lamp'
import Animal from '../../assets/SVG/Categories/Animal'
import Bike from '../../assets/SVG/Categories/Bike'
import Work from '../../assets/SVG/Categories/Work'
import Search from '../../assets/SVG/Search'

class Home extends Component {
    state={
        icon:[
            {title:"All categories",item: <All size = {90}/>},
            {title:"The property",item: <House size = {90}/>},
            {title:"Transport",item: <Car size = {90}/>},
            {title:"Furniture",item: <Lamp size = {90}/>},
            {title:"Animal",item: <Animal size = {90}/>},
            {title:"Sport",item: <Bike size = {90}/>},
            {title:"Work",item: <Work size = {90}/>},
        ]
    }
    render() {
        const {navigation} = this.props
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <TouchableOpacity style={styles.search}>
                 <Search/>
                    <Text style={styles.searchStyle}>
                      Search
                    </Text>
                </TouchableOpacity>
                <ScrollView>
                    <ScrollView
                                horizontal={true}
                                style={styles.scroll}
                                showsHorizontalScrollIndicator={false}
                    >

                        {this.state.icon.map((item,index) => {
                            return (<Components.Circle
                                key = {index}
                                title={item.item}
                                text={item.title}
                                index = {index}

                            />)
                        })}

                    </ScrollView>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

export default Home
