import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor:colors.white,
    },
    scroll:{
        marginLeft:0,
        // borderWidth:1,
    },
    search:{
        marginHorizontal:15,
        marginVertical:10,
        paddingHorizontal:10,
        paddingVertical:15,
        flexDirection:'row',
        alignItems:'center',
        // justifyContent:'space-around',
        backgroundColor: colors.background,
        borderRadius:10
    },
    searchStyle:{
        fontSize:14,
        paddingLeft:10,
        color:colors.second,
    }

})

export default styles
