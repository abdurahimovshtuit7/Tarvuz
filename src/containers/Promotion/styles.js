import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'


const styles = StyleSheet.create({
    container: {
        flex: 1,
        // marginVertical:10,
        backgroundColor: colors.white,
        // borderWidth:1,
        // justifyContent:'center',
        // paddingBottom:20
    },
    component: {
        // flex: 1,
        flexDirection: 'row',
        borderWidth: 1,
        marginHorizontal: 15,
        padding: 15,
        borderRadius: 10,
        borderColor: colors.border,
        backgroundColor: colors.white,
        elevation: 7,
        marginVertical:10,
    },
    image: {
        width: 40,
        height: 40,
        borderRadius: 5,
        overflow: 'hidden'
    },
    view: {
        width: '80%',
        // borderWidth: 1,
        paddingLeft: 15
    },
    text: {
        fontSize: 14,
        color: colors.primary
    },
    sum: {
        fontSize: 14,
        color: colors.second
    },
    text1:{
        paddingHorizontal:15,
        fontSize:12,
        color:colors.textGray,
        paddingTop:30
    }


})
export default styles
