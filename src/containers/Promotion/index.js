import React, {Component} from 'react';
import {Text, View, ScrollView, TouchableOpacity,Image} from 'react-native';
import {Icon} from 'native-base/src/basic/Icon/index';
import Ionicons from 'react-native-vector-icons/MaterialIcons'
import AnimatedHeader from 'react-native-animated-header';
import colors from "../../assets/styles/colors";
import Components from "../../components";
import styles from "./styles";
import {SafeAreaView} from "react-native-safe-area-context";



const PROP = [
    {
        key: '1',
        title:'Silver Package',
        cost:'4',
        array: [
           {text:'Highlight listing ',item:'for 3 days'}
       ]
    },
    {
        key: '2',
        title:'Golden Package',
        cost:'16',
        array: [
            {text:'Highlight listing ',item:'for 3 days'},
            {text:'Moving to top', item:'for 7 days'}
        ]
    },
    {
        key: '3',
        title:'Diamond package',
        cost:'26',
        array: [
            {text:'Highlight listing ',item:'for 3 days'},
            {text:'Moving to top', item:'for 7 days'},
            {text:'Get most views possible'}
        ],

    },

];

export default class App extends Component {
    state = {
    }

    render() {
        const {navigation, route} = this.props

        return (

                <AnimatedHeader
                    style={{flex: 1, backgroundColor:colors.white,}}
                    // backText='Back'
                    title='Promotion'
                    parallax={true}
                    renderLeft={() => (<Ionicons name="arrow-back" size={25} style={{marginLeft: 15}} onPress={() => {
                        navigation.goBack()
                    }}/>)}

                    backStyle={{marginLeft: 10}}
                    // backTextStyle={{fontSize: 14, color: '#000'}}
                    titleStyle={{fontSize: 30, left: 25, bottom: 5, color: colors.primary, fontWeight: 'bold'}}
                    headerMaxHeight={120}
                    // imageSource={()=>{
                    //
                    // }}
                    toolbarColor='#FFF'
                    disabled={false}
                    noBorder={true}
                >
                    {/*<Left>*/}
                    {/*    <Icon name='md-arrow-back' style={{marginLeft: 20}} onPress={() => {*/}
                    {/*        navigation.goBack()*/}
                    {/*    }}/>*/}
                    {/*</Left>*/}
                    <ScrollView style={styles.container}>
                    <View style={styles.component}>
                       <Image source={require('../../assets/example/img1.png')} style={styles.image}/>
                       <View style={styles.view}>
                           <Text style={styles.text} numberOfLines={1}>
                               Roommate needed, Coney Island
                           </Text>
                           <Text style={styles.sum} numberOfLines={1}>
                               $ 480
                           </Text>
                       </View>
                    </View>
                        <Text style={styles.text1}> CHOOSE PROMOTION OPTION TO GET MORE VIEWS</Text>
                     <Components.Promote PROP={PROP}/>
                      <Components.Add text={'Promote'} margin={'8%'}/>
                    </ScrollView>
                </AnimatedHeader>

        );
    }
}
