import * as React from "react";
import Svg, { Path } from "react-native-svg";

function SvgComponent(props) {
    return (
        <Svg width={12} height={12} viewBox="0 0 12 12" fill="none" {...props}>
            <Path
                d="M9.687 5.625L5.415 1.353a.521.521 0 00-.372-.153.521.521 0 00-.371.153l-.315.315a.526.526 0 000 .743l3.588 3.587-3.592 3.591a.522.522 0 00-.153.372c0 .14.055.272.153.371l.315.315c.099.098.23.153.371.153s.273-.055.372-.153L9.687 6.37a.522.522 0 00.153-.373.522.522 0 00-.153-.373z"
                fill="#738699"
            />
        </Svg>
    );
}

export default SvgComponent;
