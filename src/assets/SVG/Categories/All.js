import * as React from "react";
import Svg, { Circle, Path } from "react-native-svg";

const SvgComponent = (props) => {
    return (
        <Svg width={props.size} height={props.size} viewBox="0 0 40 40" fill="none" {...props}>
            <Circle cx={20} cy={20} r={19.5} fill="#fff" stroke="#D8E3E8" />
            <Path
                d="M16.667 15a1.667 1.667 0 11-3.334 0 1.667 1.667 0 013.334 0zM16.667 20a1.667 1.667 0 11-3.334 0 1.667 1.667 0 013.334 0zM15 26.667a1.667 1.667 0 100-3.334 1.667 1.667 0 000 3.334zM21.667 15a1.667 1.667 0 11-3.334 0 1.667 1.667 0 013.334 0zM20 21.667a1.667 1.667 0 100-3.334 1.667 1.667 0 000 3.334zM21.667 25a1.667 1.667 0 11-3.334 0 1.667 1.667 0 013.334 0zM25 16.667a1.667 1.667 0 100-3.334 1.667 1.667 0 000 3.334zM26.667 20a1.667 1.667 0 11-3.334 0 1.667 1.667 0 013.334 0zM25 26.667a1.667 1.667 0 100-3.334 1.667 1.667 0 000 3.334z"
                fill="#738699"
            />
        </Svg>
    );
}

export default SvgComponent;
