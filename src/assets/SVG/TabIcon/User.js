import * as React from "react";
import Svg, { G, Path, Defs, ClipPath } from "react-native-svg";

const SvgComponent = props => {
    return (
        <Svg width={20} height={20} viewBox="0 0 20 20" fill="none" {...props}>
            <G clipPath="url(#clip0)" fill={props.color}>
                <Path d="M10 7.484a.848.848 0 000 1.695.848.848 0 000-1.695z" />
                <Path d="M17.07 2.93a9.992 9.992 0 00-14.139 0 9.992 9.992 0 000 14.139 9.992 9.992 0 0014.139 0 9.992 9.992 0 000-14.139zm-3.733 11.228a.821.821 0 01-.82-.822A2.519 2.519 0 0010 10.821a2.519 2.519 0 00-2.516 2.515.821.821 0 01-1.643 0 4.16 4.16 0 012.097-3.61C6.826 8.086 8.002 5.841 10 5.841s3.175 2.245 2.061 3.885a4.16 4.16 0 012.098 3.61.821.821 0 01-.822.822z" />
            </G>
            <Defs>
                <ClipPath id="clip0">
                    <Path fill="#fff" d="M0 0H20V20H0z" />
                </ClipPath>
            </Defs>
        </Svg>
    );
}

export default SvgComponent;
