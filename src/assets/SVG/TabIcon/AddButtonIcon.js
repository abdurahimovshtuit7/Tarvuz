import React from 'react'

import Svg, {Rect, G, Path, Defs, LinearGradient, Stop, RadialGradient, Line} from 'react-native-svg'

export default function () {
    return <Svg xmlnsXlink="http://www.w3.org/1999/xlink" width={45} height={48}><Defs><LinearGradient id="a" x1="0.5" x2="0.5" y2={1} gradientUnits="objectBoundingBox"><Stop offset={0} stopColor="#008dff" /><Stop offset={1} stopColor="#0068ff" /></LinearGradient></Defs><G data-name="Group 412" transform="translate(0)"><G data-name="Group 411"><Rect data-name="Rectangle 248" width={45} height={45} rx="22.5" transform="translate(0 3)" fill="#0e4ce1" /><Rect data-name="Rectangle 233" width={45} height={45} rx="22.5" fill="url(#a)" /></G><G data-name="Group 410" transform="translate(15.315 15.315)" opacity="0.9"><Line data-name="Line 1" x2="14.37" transform="translate(0 7.185)" fill="none" stroke="#fff" strokeLinecap="round" strokeWidth={3} /><Line data-name="Line 2" x2="14.37" transform="translate(7.185) rotate(90)" fill="none" stroke="#fff" strokeLinecap="round" strokeWidth={3} /></G></G></Svg>;
}
