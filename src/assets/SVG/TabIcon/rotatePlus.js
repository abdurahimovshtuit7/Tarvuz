import * as React from "react";
import Svg, { Path } from "react-native-svg";

const SvgComponent = props => {
    return (
        <Svg width={20} height={20} viewBox="0 0 20 20" fill="none" {...props}>
            <Path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M14.95 5.05a1 1 0 010 1.414L6.464 14.95a1 1 0 11-1.414-1.414l8.486-8.486a1 1 0 011.414 0z"
                fill="#2469F2"
            />
            <Path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M5.05 5.05a1 1 0 011.414 0l8.486 8.486a1 1 0 01-1.414 1.414L5.05 6.464a1 1 0 010-1.414z"
                fill="#2469F2"
            />
        </Svg>
    );
}

export default SvgComponent;
