import * as React from "react";
import Svg, { Path } from "react-native-svg";

const SvgComponent = props => {
    return (
        <Svg width={14} height={14} viewBox="0 0 14 14" fill="none" {...props}>
            <Path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M7 0a1 1 0 011 1v12a1 1 0 11-2 0V1a1 1 0 011-1z"
                fill={props.color}
            />
            <Path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M0 7a1 1 0 011-1h12a1 1 0 110 2H1a1 1 0 01-1-1z"
                fill={props.color}
            />
        </Svg>
    );
}

export default SvgComponent;
