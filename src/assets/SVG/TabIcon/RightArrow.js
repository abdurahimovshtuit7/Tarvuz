import * as React from "react";
import Svg, { Path } from "react-native-svg";

const SvgComponent = props => {
    return (
        <Svg width={18} height={18} viewBox="0 0 18 18" fill="none" {...props}>
            <Path
                d="M3.47 8.438L9.878 2.03a.782.782 0 01.557-.23c.211 0 .409.082.557.23l.472.472a.789.789 0 010 1.114L6.083 8.997l5.387 5.387c.148.149.23.346.23.557a.783.783 0 01-.23.557l-.472.472a.782.782 0 01-.557.23.782.782 0 01-.557-.23L3.47 9.556a.783.783 0 01-.23-.559c0-.212.081-.41.23-.559z"
                fill={props.color}
            />
        </Svg>
    );
}

export default SvgComponent;
