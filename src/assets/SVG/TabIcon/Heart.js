import * as React from "react";
import Svg, { G, Path, Defs, ClipPath } from "react-native-svg";

const SvgComponent = props => {
    return (
        <Svg width={20} height={20} viewBox="0 0 20 20" fill="none" {...props}>
            <G clipPath="url(#clip0)">
                <Path
                    d="M14.071 1.56a5.908 5.908 0 00-4.07 1.62 5.908 5.908 0 00-4.07-1.62c-5.471 0-9.571 7.504-.996 14 2.38 1.804 4.727 2.791 4.826 2.832a.625.625 0 00.48 0c.1-.04 2.447-1.028 4.827-2.832 8.562-6.486 4.484-14-.997-14z"
                    fill={props.color}
                />
            </G>
            <Defs>
                <ClipPath id="clip0">
                    <Path fill="#fff" d="M0 0H20V20H0z" />
                </ClipPath>
            </Defs>
        </Svg>
    );
}

export default SvgComponent;
