import * as React from "react";
import Svg, { G, Path, Defs, ClipPath } from "react-native-svg";

const SvgComponent = props => {
    return (
        <Svg width={20} height={20} viewBox="0 0 20 20" fill="none" {...props}>
            <G clipPath="url(#clip0)">
                <Path
                    d="M19.817 9.244L12.044 1.47a2.895 2.895 0 00-4.088 0L.183 9.244a.625.625 0 00.884.884l.495-.496v7.946c0 .992.805 1.796 1.797 1.796h3.125a.39.39 0 00.391-.39v-5.782c0-1.078.874-1.953 1.953-1.953h2.344c1.079 0 1.953.875 1.953 1.954v5.78c0 .216.175.391.39.391h3.126c.992 0 1.797-.804 1.797-1.796V9.632l.495.496a.624.624 0 00.884 0 .625.625 0 000-.884z"
                    fill= {props.color}
                />
            </G>
            <Defs>
                <ClipPath id="clip0">
                    <Path fill="#fff" d="M0 0H20V20H0z" />
                </ClipPath>
            </Defs>
        </Svg>
    );
}

export default SvgComponent;
