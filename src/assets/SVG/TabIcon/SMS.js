import * as React from "react";
import Svg, { G, Path, Defs, ClipPath } from "react-native-svg";

const SvgComponent = props => {
    return (
        <Svg width={20} height={20} viewBox="0 0 20 20" fill="none" {...props}>
            <G clipPath="url(#clip0)">
                <Path
                    d="M17.069 2.93a9.989 9.989 0 00-14.135 0C-.207 6.07-.801 10.74 1.056 14.46L.026 18.99a.821.821 0 00.983.982l4.529-1.03c6.584 3.288 14.458-1.471 14.458-8.944A9.93 9.93 0 0017.07 2.93zm-4.983 9.556H5.831a.82.82 0 110-1.642h6.255a.82.82 0 110 1.642zm2.085-3.336h-8.34a.82.82 0 010-1.642h8.34a.82.82 0 010 1.642z"
                    fill={props.color}
                />
            </G>
            <Defs>
                <ClipPath id="clip0">
                    <Path fill="#fff" d="M0 0H20V20H0z" />
                </ClipPath>
            </Defs>
        </Svg>
    );
}

export default SvgComponent;
