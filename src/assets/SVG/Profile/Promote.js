import * as React from "react";
import Svg, { G, Path, Defs, ClipPath } from "react-native-svg";

const SvgComponent = props => {
    return (
        <Svg width={10} height={11} viewBox="0 0 10 11" fill="none" {...props}>
            <G clipPath="url(#clip0)" fill="#fff">
                <Path d="M1.09 7.685a.308.308 0 000 .435l1.32 1.32c.12.12.314.12.434 0l.516-.516-1.754-1.755-.516.516zM6.42 7.933L4.258 8.951l1.478 1.477c.12.12.315.12.435 0l1.154-1.154a.308.308 0 000-.435l-.905-.906zM5.153 1.09a.308.308 0 00-.497.087L2.041 6.734l1.755 1.754 5.557-2.615a.308.308 0 00.086-.496L5.153 1.09z" />
            </G>
            <Defs>
                <ClipPath id="clip0">
                    <Path fill="#fff" transform="translate(0 .5)" d="M0 0H10V10H0z" />
                </ClipPath>
            </Defs>
        </Svg>
    );
}

export default SvgComponent;
