import * as React from "react";
import Svg, { G, Path, Defs, ClipPath } from "react-native-svg";

const SvgComponent = props => {
    return (
        <Svg width={10} height={11} viewBox="0 0 10 11" fill="none" {...props}>
            <G clipPath="url(#clip0)" fill="#F23D5B">
                <Path d="M1.25 9.25c0 .69.56 1.25 1.25 1.25h5c.69 0 1.25-.56 1.25-1.25V3h-7.5v6.25zM6.25 1.125V.5h-2.5v.625H.625v1.25h8.75v-1.25H6.25z" />
            </G>
            <Defs>
                <ClipPath id="clip0">
                    <Path fill="#fff" transform="translate(0 .5)" d="M0 0H10V10H0z" />
                </ClipPath>
            </Defs>
        </Svg>
    );
}

export default SvgComponent;
