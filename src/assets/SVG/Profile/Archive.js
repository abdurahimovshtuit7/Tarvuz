import * as React from "react";
import Svg, { Path } from "react-native-svg";

const SvgComponent = props => {
    return (
        <Svg width={10} height={11} viewBox="0 0 10 11" fill="none" {...props}>
            <Path
                d="M9.097 2.503H5.064l-.803-.954a.137.137 0 00-.11-.049H.899A.904.904 0 000 2.404V8.27c0 .499.405.903.903.903h8.194A.904.904 0 0010 8.27V3.406a.904.904 0 00-.903-.903z"
                fill="#738699"
            />
        </Svg>
    );
}

export default SvgComponent;
