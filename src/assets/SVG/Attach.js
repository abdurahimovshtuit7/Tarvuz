import * as React from "react";
import Svg, { G, Path, Defs, ClipPath } from "react-native-svg";

const SvgComponent = props => {
    return (
        <Svg width={24} height={24} viewBox="0 0 24 24" fill="none" {...props}>
            <G clipPath="url(#clip0)">
                <Path
                    d="M3.515 20.485C.59 17.561.59 12.803 3.515 9.88l7.424-7.425a1 1 0 111.415 1.414l-7.425 7.425a5.506 5.506 0 000 7.778 5.506 5.506 0 007.778 0l7.778-7.778a3.504 3.504 0 000-4.95 3.504 3.504 0 00-4.95 0l-7.07 7.071a1.502 1.502 0 000 2.122 1.502 1.502 0 002.12 0l6.718-6.718a1 1 0 111.415 1.414L12 16.95a3.504 3.504 0 01-4.95 0 3.504 3.504 0 010-4.95l7.071-7.071a5.507 5.507 0 017.778 0 5.506 5.506 0 010 7.778l-7.778 7.778c-2.924 2.925-7.682 2.925-10.606 0z"
                    fill="#738699"
                />
            </G>
            <Defs>
                <ClipPath id="clip0">
                    <Path fill="#fff" d="M0 0H24V24H0z" />
                </ClipPath>
            </Defs>
        </Svg>
    );
}

export default SvgComponent;
