import * as React from "react";
import Svg, { Path } from "react-native-svg";

const SvgComponent = props => {
    return (
        <Svg width={16} height={16} viewBox="0 0 16 16" fill="none" {...props}>
            <Path
                d="M1.52 8.19c.44.427 1.14.427 1.58 0l3.75-3.746v9.951a1.138 1.138 0 102.275 0v-9.95l3.747 3.746a1.138 1.138 0 001.608-1.609L8.792.893a1.138 1.138 0 00-1.612 0l-5.688 5.69a1.138 1.138 0 00.027 1.608z"
                fill="#fff"
            />
        </Svg>
    );
}

export default SvgComponent;
