import * as React from "react";
import Svg, { Path } from "react-native-svg";

const SvgComponent = props => {
    return (
        <Svg width={24} height={24} viewBox="0 0 24 24" fill="none" {...props}>
            <Path
                d="M20.456 23.057a2.61 2.61 0 002.607-2.606v-3.476a.868.868 0 00-.594-.823l-5.2-1.738a.87.87 0 00-.758.101l-2.21 1.474c-2.342-1.117-5.12-3.895-6.237-6.237l1.473-2.211a.87.87 0 00.101-.757l-1.737-5.2A.868.868 0 007.077.99H3.544A2.599 2.599 0 00.937 3.584c0 10.01 9.51 19.473 19.52 19.473z"
                fill="#2469F2"
            />
        </Svg>
    );
}

export default SvgComponent;
