import * as React from "react";
import Svg, { Path } from "react-native-svg";

const SvgComponent = props => {
    return (
        <Svg width={24} height={24} viewBox="0 0 24 24" fill="none" {...props}>
            <Path d="M8 7a1 1 0 000 2h8a1 1 0 100-2H8z" fill="#3255F5" />
            <Path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M3 6a3 3 0 013-3h12a3 3 0 013 3v12a3 3 0 01-3 3H6a3 3 0 01-3-3V6zm3-1h12a1 1 0 011 1v12a1 1 0 01-1 1H6a1 1 0 01-1-1V6a1 1 0 011-1z"
                fill="#3255F5"
            />
        </Svg>
    );
}

export default SvgComponent;
