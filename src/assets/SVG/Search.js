import * as React from "react";
import Svg, { Path } from "react-native-svg";

function SvgComponent(props) {
    return (
        <Svg width={14} height={14} viewBox="0 0 14 14" fill="none" {...props}>
            <Path
                d="M10.274 9.037a5.649 5.649 0 001.1-3.35A5.694 5.694 0 005.688 0 5.694 5.694 0 000 5.687a5.694 5.694 0 005.687 5.688 5.65 5.65 0 003.35-1.101L12.763 14 14 12.763l-3.726-3.726zm-4.587.588A3.942 3.942 0 011.75 5.687 3.942 3.942 0 015.687 1.75a3.942 3.942 0 013.938 3.937 3.942 3.942 0 01-3.938 3.938z"
                fill="#738699"
            />
        </Svg>
    );
}

export default SvgComponent;
