import * as React from "react";
import Svg, { G, Path, Defs, ClipPath } from "react-native-svg";

const SvgComponent = props => {
    return (
        <Svg width={10} height={10} viewBox="0 0 10 10" fill="none" {...props}>
            <G clipPath="url(#clip0)">
                <Path
                    d="M9.792 7.499L6.26 1.005a1.465 1.465 0 00-2.52 0L.208 7.5a1.464 1.464 0 001.26 2.213h7.065a1.464 1.464 0 001.26-2.213zM5 8.539a.587.587 0 11.002-1.173A.587.587 0 015 8.54zm.586-2.343a.587.587 0 01-1.172 0v-2.93a.587.587 0 011.172 0v2.93z"
                    fill="#738699"
                />
            </G>
            <Defs>
                <ClipPath id="clip0">
                    <Path fill="#fff" d="M0 0H10V10H0z" />
                </ClipPath>
            </Defs>
        </Svg>
    );
}

export default SvgComponent;
