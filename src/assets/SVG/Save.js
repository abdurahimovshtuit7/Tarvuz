import * as React from "react";
import Svg, { G, Path, Defs, ClipPath } from "react-native-svg";
/* SVGR has dropped some elements not supported by react-native-svg: filter */

const SvgComponent = props => {
    return (
        <Svg width={24} height={24} viewBox="0 0 24 24" fill={props.color} fillOpacity={props.opacity} {...props}>
            <G clipPath="url(#clip0)">
                <Path fill="#000" fillOpacity={0.01}  d="M0 0H24V24H0z"/>
                <G filter="url(#filter0_d)">
                    <Path
                        d="M16.886 1.872c-1.83 0-3.57.7-4.884 1.945a7.09 7.09 0 00-4.884-1.945c-6.566 0-11.486 9.005-1.196 16.8 2.857 2.165 5.673 3.35 5.792 3.399a.75.75 0 00.576 0c.119-.05 2.935-1.234 5.792-3.398 10.273-7.784 5.381-16.801-1.197-16.801z"
                        fill="#181818"
                        fillOpacity={0.1}
                    />
                    <Path
                        d="M11.314 4.543l.688.651.687-.65a6.103 6.103 0 0110.14 3.04c.643 2.722-.42 6.555-5.351 10.291a28.697 28.697 0 01-5.476 3.232 28.7 28.7 0 01-5.476-3.232C1.586 14.133.527 10.3 1.172 7.58a6.11 6.11 0 015.946-4.707 6.09 6.09 0 014.196 1.671z"
                        stroke="#fff"
                        strokeWidth={2}
                    />
                </G>
            </G>
            <Defs>
                <ClipPath id="clip0">
                    <Path fill="#fff" d="M0 0H24V24H0z" />
                </ClipPath>
            </Defs>
        </Svg>
    );
}

export default SvgComponent;
