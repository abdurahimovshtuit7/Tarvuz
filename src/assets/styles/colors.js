const brandColor = "#2469F2";
const white = "#fff"
const border = '#D8E3E8'
const gray ="#F5F8FA"
const primary = "#001D38"
const textGray = '#738699'
const view = '#F0F3F6'
const second ='#738699'
const background = '#F0F3F5'
const property ='rgba(88, 144, 255, 0.2)'
const transport = 'rgba(255, 212, 100, 0.2)'
const lamp ='rgba(175, 95, 255, 0.2)'
const animal = 'rgba(209, 172, 138, 0.2)'
const sport ='rgba(107, 227, 235, 0.2)'
const work = 'rgba(169, 139, 122, 0.2)'
const backgroundColor = '#DEE9FF'
const red ='#F23D5B'
const green ='#27AE60'
const title ='#5890FF'

export default {
    view,
    brandColor,
    white,
    border,
    gray,
    primary,
    textGray,
    second,
    background,
    property,
    transport,
    lamp,
    animal,
    sport,
    work,
    backgroundColor,
    red,
    green,
    title,
    backgroundTransparent: 'transparent',
    mainColor: "#0550ea",
    carrot: '#e67e22',
    emerald: '#2ecc71',
    peterRiver: '#3498db',
    wisteria: '#8e44ad',
    alizarin: '#e74c3c',
    turquoise: '#1abc9c',
    midnightBlue: '#2c3e50'
};

