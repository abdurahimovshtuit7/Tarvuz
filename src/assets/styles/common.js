export const verticalCenter = {
    justifyContent: 'center'
};

export const horizontalCenter = {
    alignItems: 'center'
};

export const center = {
    ...verticalCenter,
    ...horizontalCenter
};

export const horizontal = {
    flexDirection: 'row',
    alignItems: 'center'
};

export const shadow = {
    //ios
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowOffset: {
        height: 0,
        width: 0
    },
    //android
    elevation: 1
};

export const container = {
    flex:1
};
